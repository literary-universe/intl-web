export default {
    'feed.name': 'Ström',
    'feed.post.new': 'Nytt inlägg',
    'feed.post.like': 'Gilla inlägg',
    'feed.post.unlike': 'Till skillnad från inlägget',
    'feed.post.send': 'Inlägg',
    'feed.empty': 'Inga poster i matningen.'
};