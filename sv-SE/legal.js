export default {
    'legal.modal.intro': 'Ursäkta att jag avbryter. Men innan vi låter dig gå vidare är vi juridiskt förpliktade att se till att du känner till och godkänner våra användarvillkor, vår sekretesspolicy och vårt upphovsrättsavtal.',
    'legal.modal.agree': 'Jag samtycker',
    'legal.modal.disagree': 'Jag godkänner inte',
    'legal.modal.register': 'Du kommer inte att se detta längre om du registrerar dig.',
    'legal.modal.goToSettings': 'Gå till dina inställningar',
    'legal.modal.fewmore': 'Några fler saker...',
    'legal.modal.finish': 'Slutförande'
};