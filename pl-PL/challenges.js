export default {
    'challenges.current': 'Bieżące wyzwania',
    'challenges.wordcount.goal': 'Docelowa liczba słów',
    'challenges.wordcount.dailygoal': 'Docelowa liczba słów dziennie',
    'challenges.wordcount.today': 'Napisanych dzisiaj',
    'challenges.wordcount.total': 'Łącznie napisanych słów',
    'challenges.wordcount.remaining': 'Pozostało słów',
    'challenges.wordcount.daysleft': 'Pozostało dni',
    'challenges.wordcount.currentDay': 'Bieżący dzień',
    'challenges.wordcount.average': 'Średnia ilość słów dziennie',
    'challenges.wordcount.finishOn': 'Przy obecnej szybkości ukończysz w dniu',
    'challenges.wordcount.finishOnTime': 'Słów do napisania w celu ukończenia na czas',
    'challenges.wordcount.graph.day': 'Dzień {day}',
    'challenges.underway': 'To wyzwanie zostało już podjęte.',
    'challenges.targetWords': 'Łączna docelowa liczba słów',
    'challenges.startDate': 'Data rozpoczęcia',
    'challenges.endDate': 'Data zakończenia',
    'challenges.future': 'Nadchodzące wyzwania',
    'challenges.future.wordcount': 'Wyzwanie liczby słów rozpocznie się w dniu {startDate, date, short} z liczbą docelową {target, number} i potrwa do dnia {endDate, date, short}.',
    'challenges.future.nanowrimo': 'NaNoWriMo challenge is ready.',
    'challenges.wordcount': 'Liczba słów',
    'challenges.wordcount.desc': `Set yourself a goal of how many words you want to write this month for this {type, select,
    story {story}
    universe {universe}
    other {}
  }.`,
    'challenges.wordcount.select': 'Jak wiele słów chcesz sobie narzucić?',
    'challenges.start': 'Rozpocznij wyzwanie',
    'challenges.report': 'Zgłoś',
    'challenges.victory': 'Gratulacje! To wyzwanie zostało ukończone.',
    'challenges.defeat': 'Niestety, ale nie ukończono tego wyzwania.',
    'challenges.baseline': 'The current wordcount for this story is {wordcount, number}, this will be the starting point.',
    // NaNoWriMo
    'nanowrimo.full': 'National Novel Writing Month',
    'nanowrimo.site': 'Oficjalna strona NaNoWriMo',
    'nanowrimo.settings': 'Konto NaNoWriMo',
    'nanowrimo.username': 'Nazwa użytkownika NaNoWriMo',
    'nanowrimo.key': 'Tajny klucz',
    'nanowrimo.key.get': 'Get your NaNoWriMo key.',
    'nanowrimo.key.get.notice': 'You need to be logged in to see it.',
    'nanowrimo.ongoing': 'is in full swing! {countdown} more days to go!',
    'nanowrimo.startsin': `will start in {countdown, plural,
      one {1 day}
      other {# days}
    }! Get ready!`,
    'nanowrimo.isover': 'NaNoWriMo zakończył się. Zobaczcie w przyszłym roku!',
    'nanowrimo.select': 'Wyznacz tę opowieść dla NaNoWriMo.',
    'nanowrimo.baseline': 'When NaNoWriMo starts it will take the current wordcount and use it as a baseline from which any additional words will be taken as part of NaNoWriMo.',
    'nanowrimo.victory': 'Congratulation! You won NaNoWriMo!',
    'nanowrimo.challenge': 'NaNoWriMo challenge',
    'nanowrimo.description': 'The challenge is to write a novel of {words, number} words during November.',
    'nanowrimo.error.usernotfound': 'Username has not been found on the NaNoWriMo servers.',
    'nanowrimo.error.nobook': 'No book has been found. Please make sure you have created your book on NaNoWriMo.',
    'nanowrimo.error.wordcount': 'Unable to set wordcount on NaNoWrimo, please double check your credentials and try again.',
    'nanowrimo.connected': 'You have designated this story for NaNoWriMo.',
    'nanowrimo.connected.other': 'You have already connected another story.'
};