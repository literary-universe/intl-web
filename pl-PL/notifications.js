// Notifications & flashnews
export default {
  'flashnews.create': 'Utwórz nowy flesz informacyjny',
  'flashnews.startsAt': 'Zacznij wyświetlać tę wiadomość na',
  'flashnews.endsAt': 'Zatrzymaj wyświetlanie tej wiadomości na',
  'flashnews.newLanguage': 'Wybierz nowy język do dodania',
  'flashnews.onlyDisplayOn': 'Wiadomości będą wyświetlane tylko w językach wybranych poniżej (jeśli zostały wybrane), w innych językach nie będą wyświetlane żadne wiadomości, nawet w języku domyślnym.'
};