export default {
    'user.friend.request.add': 'Dodaj do znajomych',
    'user.friend.requests': `You have {num, number} new friend {num, plural,
    zero {requests}
    one {request}
    many {requests}
    other {requests}
  }`,
    'user.friend.unfriend': 'Usuń ze znajomych',
    'user.block': 'Zablokuj',
    'user.unblock': 'Odblokuj',
    'user.joined': `{gender, select,
    male {Dołączył}
    female {Dołączyła}
    other {Dołączyli}} w dniu {date, date, long}`,
    'user.friend.request.accept': 'Zaakceptuj zaproszenie do znajomych',
    'user.friend.request.cancel': 'Anuluj zaproszenie do znajomych',
    'user.friend.request.deny': 'Odrzuć zaproszenie',
    'user.listing': 'Lista użytkowników',
    'user.profile.visit': 'Odwiedź profil {user}.',
    'user.profile.avatar': 'Awatar {username}',
    'user.friend.list.requests': 'Zaproszenia do znajomych',
    'user.friend.norequests': 'Nie masz żadnych zaproszeń do znajomych.',
    'user.dashboardWelcome': `{dayPart, select,
    morning {Dzień dobry}
    afternoon {Miłego popołudnia}
    evening {Dobry wieczór}
    night {Dobrej nocy}
    other {Dzień dobry}
  } {displayName}!`,
    'user.verification.publishedAuthor': 'Ten autor został opublikowany jako tradycyjny sposób',
    'user.verification.luAuthor': 'Autorka z Literackiego Wszechświata',
    'user.verification.luEmployee': 'Pracownik Wszechświata Literackiego',
    'user.verification.publicFigure': 'Zweryfikowana osoba publiczna'
};