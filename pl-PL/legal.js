export default {
    'legal.modal.intro': 'Przepraszamy za przeszkadzanie, ale przed kontynuowaniem jesteśmy prawnie zobligowani, aby uświadomić Cię o naszych Warunkach użytkowania, Polityce prywatności i Umowie o prawach autorskich oraz wymagamy wyrażenia przez Ciebie zgody.',
    'legal.modal.agree': 'Wyrażam zgodę',
    'legal.modal.disagree': 'Nie wyrażam zgody',
    'legal.modal.register': 'Nie zobaczysz tego więcej, jeśli się zarejestrujesz.',
    'legal.modal.goToSettings': 'Przejdź do ustawień',
    'legal.modal.fewmore': 'Jeszcze kilka rzeczy...',
    'legal.modal.finish': 'Gotowe'
};