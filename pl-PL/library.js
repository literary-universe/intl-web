export default {
    'library.add': 'Dodaj do biblioteczki',
    'library.remove': 'Usuń z biblioteczki',
    'library.bought': 'Zakupione opowieści',
    'library.stories': 'Obserwowane opowieści',
    'library.universes': 'Podążanie za wszechświatami'
};