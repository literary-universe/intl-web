export default {
    'comment.write': 'Напишіть свій коментар',
    'comments.none': 'Вибачте, жодних коментарів для показу.',
    'comments.total': `{count, plural,
    =0 {Немає коментарів}
    one {є 1 коментар}
    few {# коментарі}
    many {# коментарі}
    other {# коментарі}
  }`,
    'comments.show.older': 'Показати старі коментарі'
};