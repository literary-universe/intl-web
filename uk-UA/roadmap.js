export default {
  'roadmap.ariaTimeOptions': 'Вибір пунктів дорожньої карти на основі їх статусу.',
  'roadmap.state.backlog': 'Відставання',
  'roadmap.state.inprogress': 'У процесі виконання',
  'roadmap.state.preview': 'Перегляд',
  'roadmap.state.beta': 'Beta',
  'roadmap.state.ga': 'Загальна доступність',
  'roadmap.eta.q1.short': 'Q1',
  'roadmap.eta.q2.short': 'Q2',
  'roadmap.eta.q3.short': 'Q3',
  'roadmap.eta.q4.short': 'Q4',
  'roadmap.eta.q1.long': 'Перший квартал',
  'roadmap.eta.q2.long': 'Другий квартал',
  'roadmap.eta.q3.long': 'Третій квартал',
  'roadmap.eta.q4.long': 'Четвертий квартал',
  'roadmap.backlog': 'Backlog',
  'roadmap.history': 'Історія розвитку',
  'roadmap.item.inBacklog': 'Ми плануємо розвивати цю функцію в майбутньому.',
  'roadmap.item.inProgress': 'The development of the feature started {startedAt, date}.',
  'roadmap.item.inBeta': 'This feature has entered beta on {betaAt, date}.',
  'roadmap.item.inPreview': 'This feature became available to Storyteller subscribers on {previewAt, date}.',
  'roadmap.item.inGa': 'This feature has become generally available on {gaAt, date}.',
  'roadmap.item.originalIssue': 'This feature was originally suggested by our users.',
  'roadmap.eta.mapTitle': `Planned for {quarter, select,
    Q1 {Q1}
    Q2 {Q2}
    Q3 {Q3}
    Q4 {Q4}
    other {}
  } {year, date, ::yyyy}`,
  'roadmap.eta.mapTitleFinished': 'Finished in {time, date, ::MMMM ::yyyy}',
  'roadmap.back': 'Back to roadmap',
  'roadmap.seeBacklog': 'Check out what other things we plan to do',
  'roadmap.seeFinished': 'If you like to dig more into our history, we have a nice overview ready for you',
  'roadmap.planned': 'Planned',
  'roadmap.finished': 'Finished'
};