export default {
    'discovery.meta.desc': 'Відкрийте для себе нові історії та світи.',
    'discovery.universes.new': 'Виникаючі світи',
    'discovery.stories.new': 'Нові історії',
    'discovery.stories.standalone.new': 'Нові автономні історії',
    'discovery.stories.updated': 'Нещодавно оновлені історії',
    'discovery.continue': 'Продовжити читання',
    'discovery.stories.similar.new': 'New stories in the neighborhood',
    'discovery.stories.similar.regular': 'Stories in the neighborhood',
    'story.estimatedReadingTime': `This story should take {hours, plural,
        =0 {}
        one {1 hour and }
        other {# hours and }
    }{minutes, plural,
        =0 {0 minutes}
        one {1 minute}
        other {# minutes}
    } to read.`,
    'story.estimatedReadingTime.explained': 'Based on an average reading speed of {avgReadingNum} words per minute.',
    'discover.search.text': 'Search term',
    'discover.searchStory.title': 'Search for a story',
    'discover.searchStory.description': 'Search for a stories',
    'discover.searchUniverse.title': 'Search for a universe',
    'discover.searchUniverse.description': 'Search for a universes',
    'discover.search.storyStatus': 'Story progress status',
    'story.status.complete': 'Complete',
    'story.status.inProgress': 'In-progress',
    'discover.search.universeConnection': 'Connection of story to universe',
    'story.universeRelation.standalone': 'Автономна історія',
    'story.universeRelation.fanFiction': 'Фанфік',
    'story.universeRelation.universe': 'Частина всесвіту',
    'discover.search.rating': 'Віковий рейтинг',
    'discover.search.license': 'Авторські права',
    'discover.search.language': 'Мова',
    'discover.search.resultsTotal.universe': `{results, plural,
        =0 {No universes match your parameters}
        one {1 universe found}
        other {# universes found}
    }.`,
    'discover.search.resultsTotal.story': `{results, plural,
        =0 {No stories match your parameters}
        one {1 story found}
        other {# stories found}
    }.`,
    'discover.viewSelection.aria': 'View selection',
    'discover.searchDisplay.covers': 'Covers listing',
    'discover.searchDisplay.list': 'Компактний список',
    'discover.includesAds': 'This work has product placement or includes other form of advertisements in it.'
};