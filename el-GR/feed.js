export default {
    'feed.name': 'Ροή',
    'feed.post.new': 'Νέα θέση',
    'feed.post.like': 'Όπως η θέση',
    'feed.post.unlike': 'Σε αντίθεση με τη θέση',
    'feed.post.send': 'Θέση',
    'feed.empty': 'Δεν υπάρχουν καταχωρήσεις στην τροφοδοσία.'
};