export default {
    'form.requiredSymbol': '*',
    'form.validator.required': 'Υποχρεωτικό πεδίο',
    'form.validator.mustBeNumber': 'Πρέπει να είναι αριθμός',
    'form.validator.minNumber': 'Πρέπει να είναι μεγαλύτερη από {min, number}',
    'form.validator.maxNumber': 'Θα πρέπει να είναι μικρότερη από {max, number}',
    'form.validator.maxLength': `Should be less than {max, number} {what, select,
    characters {characters}
    tags {tags}
    genres {genres}
    numbers {numbers}
    other {}
  }`,
    'form.validator.minLength': `Should be at least {min, number} {what, select,
    characters {characters}
    tags {tags}
    genres {genres}
    genre {genre}
    numbers {numbers}
    other {}
  }`,
    'form.validator.email': 'Πρέπει να είναι έγκυρο e-mail',
    'form.validator.url': 'Πρέπει να είναι ένα έγκυρο URL',
    'form.validator.slug': 'Πρέπει να είναι έγκυρο τμήμα της διεύθυνσης URL',
    'form.submitting.signup': 'Δημιουργία λογαριασμού...',
    'form.submitting.signin': 'Σύνδεση...',
    'form.submitting.default': 'Υποβολή...',
    'form.submitting.sending': 'Στέλνοντας...',
    'form.validator.whitespace': 'Δεν πρέπει να έχει λευκό χώρο',
    'form.validator.zipCode': 'Πρέπει να είναι έγκυρος ταχυδρομικός κώδικας',
    'form.validator.isbn': 'Δεχόμαστε μόνο 10 ή 13 έκδοση του ISBN',
    'form.validator.btc': 'This is not a valid BTC address.',
    'form.validator.eth': 'This is not a valid ETH address.',
    'form.weakPassword': 'This password looks weak. Please consider making it stronger by adding numbers and special characters.',
    // you can get these messages from: https://github.com/gwendall/meteor-simple-schema-i18n/tree/master/i18n
    // but don't forget to adjust the format
    'simpleschema.required': 'Το πεδίο {label} είναι απαραίτητο',
    'simpleschema.minString': '{label} must be at least {min, number} characters',
    'simpleschema.maxString': '{label} cannot exceed {max, number} characters',
    'simpleschema.minNumber': '{label} must be at least {min, number}',
    'simpleschema.maxNumber': '{label} cannot exceed {max, number}',
    'simpleschema.minNumberExclusive': '{label} must be greater than {min, number}',
    'simpleschema.maxNumberExclusive': '{label} must be less than {max, number}',
    'simpleschema.minDate': '{label} must be on or after {min, date}',
    'simpleschema.maxDate': '{label} cannot be after {max, date}',
    'simpleschema.badDate': 'Το πεδίο {label} δεν είναι έγκυρη ημερομηνία',
    'simpleschema.minCount': 'You must specify at least {minCount, number} values',
    'simpleschema.maxCount': 'You cannot specify more than {maxCount, number} values',
    'simpleschema.noDecimal': 'Το πεδίο {label} πρέπει να είναι ακέραιος αριθμός',
    'simpleschema.notAllowed': 'To {value} δεν είναι αποδεκτή τιμή',
    'simpleschema.expectedString': 'Το πεδίο {label} πρέπει να είναι μια σειρά χαρακτήρων',
    'simpleschema.expectedNumber': 'Το πεδίο {label} πρέπει να είναι ένας αριθμός',
    'simpleschema.expectedBoolean': 'Το πεδίο {label} πρέπει να είναι true ή false',
    'simpleschema.expectedArray': 'Το πεδίο {label} πρέπει να είναι ένας πίνακας',
    'simpleschema.expectedObject': 'Το πεδίο {label} πρέπει να είναι ένα αντικείμενο',
    'simpleschema.expectedConstructor': 'Το πεδίο {label} πρέπει να είναι ένα {type}',
    'simpleschema.keyNotInSchema': 'Το πεδίο {key} δεν είναι επιτρεπτό',
    'simpleschema.notUnique': '{label} πρέπει να είναι μοναδικό',
    'simpleschema.regEx.0': 'Η μορφή του πεδίου {label} δεν είναι αποδεκτή',
    'simpleschema.regEx.1': 'Το πεδίο {label} πρέπει να είναι μια έγκυρη διεύθυνση e-mail',
    'simpleschema.regEx.2': 'Το πεδίο {label} πρέπει να είναι μια έγκυρη διεύθυνση e-mail',
    'simpleschema.regEx.3': 'Το πεδίο {label} πρέπει να είναι ένα έγκυρο domain',
    'simpleschema.regEx.4': 'Το πεδίο {label} πρέπει να είναι ένα έγκυρο domain',
    'simpleschema.regEx.5': 'Το πεδίο {label} πρέπει να είναι μια έγκυρη διεύθυνση IPv4 ή IPv6',
    'simpleschema.regEx.6': 'Το πεδίο {label} πρέπει να είναι μια έγκυρη διεύθυνση IPv4',
    'simpleschema.regEx.7': 'Το πεδίο {label} πρέπει να είναι μια έγκυρη διεύθυνση IPv6',
    'simpleschema.regEx.8': 'Το πεδίο {label} πρέπει να είναι μια έγκυρη διεύθυνση URL',
    'simpleschema.regEx.9': 'Το πεδίο {label} πρέπει να είναι ένα έγκυρο ID'
};