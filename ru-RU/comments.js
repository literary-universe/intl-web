export default {
    'comment.write': 'Напишите свой комментарий',
    'comments.none': 'Извините, комментариев нет.',
    'comments.total': `There {count, plural,
    =0 {are no comments}
    one {is 1 comment}
    other {are # comments}
  }.`,
    'comments.show.older': 'Показать старые комментарии'
};