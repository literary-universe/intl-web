export default {
    'blog.noneMsg': `You currently don't have {type, select,
    organization {any blog}
    universe {a blog for your universe}
    user {a blog}
    other {}
  }.`,
    'blog.create': 'Tạo một blog mới',
    'blog.settings': `{type, select,
    org {Blog}
    universe {Universe page}
    user {Blog}
    other {}
  } settings`,
    'blog.description': 'Giới thiệu về Blog',
    'blog.theme': 'Giao diện',
    'blog.theme.info': 'Điều chỉnh chủ đề, bạn sẽ có thể thay đổi giao diện của trang. Tính năng này hiện chưa có vào lúc này.',
    'blog.posts.total': `There {total, plural,
    zero {are no posts}
    one {is one post}
    other {are # posts}
  } in this blog.`,
    'blog.post.create': 'Tạo bài viết mới',
    'blog.post.update': 'Đang chỉnh sửa {title}',
    'blog.post.text': 'Post text',
    'common.slug': 'SEO friendly address',
    'blog.slug.explained': 'Nice looking url to the post. Avoid using special characters except for "-".',
    'blog.publicView': 'Public view',
    'blog.post.notfound': 'Blog post was not found',
    'blog.lists.works': `{type, select,
    universes {Universes}
    stories {Standalone stories}
    fanfiction {Fan Fiction work}
    other {}
  }`,
    'blog.lists.stories.disclaimer': 'Listed here are standalone stories, if story was written as a part of a universe then it will be listed on the universe page.',
    'blog.social.sameAsMain': `Social links are the same as set in {type, select,
    organization {organization settings}
    user {user profile}
    other {}
  }`,
    'blog.social.website': 'Trang web chính thức',
    'blog.settings.universeSettings': 'Universe blogs are integrated into universe pages with their basic settings taken from universe settings.',
    'blog.settings.organizationSettings': 'Organization blogs are integrated into organization pages.',
    'blogs.lu': 'Official Literary Universe blogs',
    'blogs.featured': 'Featured blogs',
    'blogs.new': 'Blog mới nhất',
    'blogs.title': 'Blogs of Literary Universe',
    'blogs.description': 'Listing of blogs on the Literary Universe platform.',
    'blogs.visit': 'Visit blog',
    'blog.settingsAria': 'Settings categories for the blog',
    'blog.settings.blog': 'Blog',
    'blog.settings.forum': 'Forum'
};