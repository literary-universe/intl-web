export default {
    'challenges.current': 'Current challenges',
    'challenges.wordcount.goal': 'Target word count',
    'challenges.wordcount.dailygoal': 'Target daily word count',
    'challenges.wordcount.today': 'Today written',
    'challenges.wordcount.total': 'Total words written',
    'challenges.wordcount.remaining': 'Words remaining',
    'challenges.wordcount.daysleft': 'Days remaining',
    'challenges.wordcount.currentDay': 'Current day',
    'challenges.wordcount.average': 'Average words per day',
    'challenges.wordcount.finishOn': 'At current speed you will finish on',
    'challenges.wordcount.finishOnTime': 'Words to write daily to finish on time',
    'challenges.wordcount.graph.day': 'Day {day}',
    'challenges.underway': 'This challenge is already underway.',
    'challenges.targetWords': 'Total targeted word count',
    'challenges.startDate': 'Ngày bắt đầu',
    'challenges.endDate': 'End date',
    'challenges.future': 'Thử thách sắp tới',
    'challenges.future.wordcount': 'Wordcount challenge staring on {startDate, date, short} with goal of {target, number} by {endDate, date, short}.',
    'challenges.future.nanowrimo': 'NaNoWriMo challenge is ready.',
    'challenges.wordcount': 'Word count',
    'challenges.wordcount.desc': `Set yourself a goal of how many words you want to write this month for this {type, select,
    story {story}
    universe {universe}
    other {}
  }.`,
    'challenges.wordcount.select': 'How many words do you want to challenge yourself to?',
    'challenges.start': 'Bắt đầu thử thách',
    'challenges.report': 'Báo cáo',
    'challenges.victory': 'Chúc mừng! Bạn đã hoàn thành thử thách này.',
    'challenges.defeat': 'Sadly you didn\'t complete this challenge.',
    'challenges.baseline': 'The current wordcount for this story is {wordcount, number}, this will be the starting point.',
    // NaNoWriMo
    'nanowrimo.full': 'National Novel Writing Month',
    'nanowrimo.site': 'NaNoWriMo official site',
    'nanowrimo.settings': 'NaNoWriMo account',
    'nanowrimo.username': 'NaNoWriMo username',
    'nanowrimo.key': 'Khóa bí mật',
    'nanowrimo.key.get': 'Lấy khóa NaNoWriMo của bạn.',
    'nanowrimo.key.get.notice': 'Bạn cần phải đăng nhập để xem.',
    'nanowrimo.ongoing': 'is in full swing! {countdown} more days to go!',
    'nanowrimo.startsin': `will start in {countdown, plural,
      one {1 day}
      other {# days}
    }! Get ready!`,
    'nanowrimo.isover': 'NaNoWriMo is over. See you next year!',
    'nanowrimo.select': 'Designate this story for NaNoWriMo.',
    'nanowrimo.baseline': 'When NaNoWriMo starts it will take the current wordcount and use it as a baseline from which any additional words will be taken as part of NaNoWriMo.',
    'nanowrimo.victory': 'Congratulation! You won NaNoWriMo!',
    'nanowrimo.challenge': 'Thử thách NaNoWriMo',
    'nanowrimo.description': 'Thử thách là viết một tiểu thuyết {words, number} từ trong tháng Mười một.',
    'nanowrimo.error.usernotfound': 'Tên đăng nhập không được tìm thấy trên các máy chủ của NaNoWriMo.',
    'nanowrimo.error.nobook': 'Không có cuốn sách nào. Vui lòng chắc chắn rằng bạn đã tạo sách của mình trên NaNoWriMo.',
    'nanowrimo.error.wordcount': 'Unable to set wordcount on NaNoWrimo, please double check your credentials and try again.',
    'nanowrimo.connected': 'You have designated this story for NaNoWriMo.',
    'nanowrimo.connected.other': 'You have already connected another story.'
};