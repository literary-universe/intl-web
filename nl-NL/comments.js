export default {
    'comment.write': 'Schrijf uw reactie',
    'comments.none': 'Sorry, geen reacties om te tonen.',
    'comments.total': `There {count, plural,
    =0 {are no comments}
    one {is 1 comment}
    other {are # comments}
  }.`,
    'comments.show.older': 'Toon oudere reacties'
};