export default {
    'library.add': 'إضافة إلى المكتبة',
    'library.remove': 'إزالة من المكتبة',
    'library.bought': 'Bought stories',
    'library.stories': 'Followed stories',
    'library.universes': 'Followed universes'
};