export default {
    'user.friend.request.add': 'إضافة إلى الأصدقاء',
    'user.friend.requests': `You have {num, number} new friend {num, plural,
    zero {requests}
    one {request}
    many {requests}
    other {requests}
  }`,
    'user.friend.unfriend': 'إلغاء الصداقة',
    'user.block': 'حظر',
    'user.unblock': 'إلغاء الحظر',
    'user.joined': `{gender, select,
    male {Joined}
    female {Joined}
    other {Joined}} on {date, date, long}`,
    'user.friend.request.accept': 'قبول طلب الصداقة',
    'user.friend.request.cancel': 'إلغاء طلب الصداقة',
    'user.friend.request.deny': 'رفض الصداقة',
    'user.listing': 'User listing',
    'user.profile.visit': 'زيارة ملف {user} الشخصي.',
    'user.profile.avatar': '{username}\'s avatar',
    'user.friend.list.requests': 'طلبات الصداقة',
    'user.friend.norequests': 'ليس لديك أي طلبات للصداقة.',
    'user.dashboardWelcome': `Good {dayPart, select,
    morning {morning}
    afternoon {afternoon}
    evening {evening}
    night {night}
    other {day}
  } {displayName}!`,
    'user.verification.publishedAuthor': 'This author has been published the traditional way',
    'user.verification.luAuthor': 'Literary Universe based author of note',
    'user.verification.luEmployee': 'Literary Universe employee',
    'user.verification.publicFigure': 'Verified public figure'
};