export default {
    'feed.name': 'Diretta',
    'feed.post.new': 'Nuovo post',
    'feed.post.like': 'Metti mi piace',
    'feed.post.unlike': 'Togli mi piace',
    'feed.post.send': 'Post',
    'feed.empty': 'Nessuna voce nel feed.'
};