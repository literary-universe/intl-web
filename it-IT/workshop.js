export default {
    'workshop.title': 'Il tuo lavoro',
    'workshop.noneassigned': 'Non ti è stato ancora assegnato nessun lavoro.',
    'workshop.notavailable': 'Questa funzione non è ancora disponibile.',
    'workshop.none': `Al momento non hai nessun/a {type, select,
      universe {universo}
      story {storia}
      fanfiction {fan fiction}
      other {}
    }. Crea qualcosa!`,
    'workshop.collab': 'Collaborazioni',
    'workshop.betareadings': 'Lettori beta',
    'workshop.translations': 'Traduzioni',
    'workshop.createnew': 'Crea nuovi...',
    'workshop.story.standalone': 'Storia auto conclusiva',
    'workshop.story.standalone.desc': 'Questa è una storia auto conclusiva.',
    'workshop.controlpanel': '{name} pannello di controllo',
    'workshop.universe.page': 'Pagina dell\'Universo',
    'workshop.fictionaltime': 'Tempo immaginario',
    'workshop.dashboard.name': '{name} dashboard',
    'workshop.stories.in': 'Storie in {universe}',
    'workshop.universe.nostories': 'Non ci sono storie in questo universo.',
    'workshop.story.chapter.add': 'Nuovo capitolo',
    'workshop.universe.new': 'Creare un nuovo universo',
    'workshop.universe.publish': 'Pubblica universo',
    'workshop.universe.publishing.notice': 'Se non pubblichi l\'universo, sarà pubblicato automaticamente alla pubblicazione della prima storia connessa all\'universo.',
    'workshop.universe.settings': '{universe} impostazioni universo',
    'workshop.universe.publishedOn': 'Questo universo è stato pubblicato il {date, date, long} {date, time, short}',
    'workshop.universe.description': 'Descrizione dell\'universo',
    'workshop.settings.title': 'Titolo',
    'workshop.description': 'Descrizione',
    'workshop.language.primary': 'Lingua principale',
    'workshop.publication.options': 'Opzioni di pubblicazione',
    'workshop.rating': 'Valutazione generale',
    'workshop.universe.rating.desc': 'Storie e fan fiction al di sopra di questa classificazione saranno nascoste dalla lista dell\'universo a utenti anonimi e utenti che non sono dell\'età appropriata.',
    'workshop.universe.license': 'Licenza dell\'universo',
    'workshop.universe.genre.pick': 'Scegli il genere per l\'universo',
    'workshop.story.genre.pick': 'Scegli i generi per la storia',
    'workshop.cover': 'Copertina',
    'workshop.nothingtosee': 'Niente da vedere qui...',
    'workshop.story.cannon': 'Storia standard',
    'workshop.story.cannon.desc': 'Questa storia è standard nell\'universo {universe}.',
    'workshop.story.cannon.desc.create': 'Questa storia sarà creata come una storia standard nell\'universo {universe}.',
    'workshop.story.fanfiction.search': 'Digita il nome dell\'universo (deve essere nella tua biblioteca). Poi seleziona dai suggerimenti.',
    'workshop.story.error.nogenres': 'È necessario selezionare almeno un genere.',
    'workshop.story.error.toomanygenres': 'Sono ammessi solo due generi per storia.',
    'workshop.story.error.fanfiction.selectuniverse': 'Per la tua fan fiction devi selezionare universo originale!',
    'workshop.story.error.tags.toomany': 'Possono esserci solo 10 tag per storia.',
    'workshop.story.new': 'Crea una nuova storia',
    'workshop.story.publishing.notice': 'La storia verrà pubblicata automaticamente una volta pubblicato il primo capitolo.',
    'workshop.settings.edit': 'Modifica impostazioni',
    'workshop.story.publishedOn': 'Questa storia è stata pubblicata il {date, date, long} {date, time, short}',
    'workshop.story.settings': 'Impostazioni storia',
    'workshop.settings.basic': 'Impostazioni base',
    'workshop.settings.genres': 'Generi (max 2)',
    'workshop.story.license': 'Licenza storia',
    'workshop.story.settings.universe': 'Impostazioni dell\'universo',
    'workshop.settings.tags': 'Tag',
    'workshop.revision': 'Revisione',
    'workshop.revisions.title': '{story}: {chapter} Revisioni',
    'workshop.revisions.current': 'Progetto attuale',
    'workshop.revisions.new.beta': 'Crea una nuova revisione dal progetto.',
    'workshop.revisions.new.public': 'Crea una nuova revisione pubblica dal progetto.',
    'workshop.revisions.toeditor': 'Vai all\'editor',
    'workshop.revisions.comments': 'Commenti sulla revisione',
    'workshop.revisions.comments.title': '{story}: {chapter} - Revisione {revision} - Commenti',
    'workshop.revisions.comments.list': '{revision}: {publication, select, beta {per i lettori Beta} readers {Pubblico} other {}} {datetime, date, medium} {datetime, time, short}',
    'workshop.revisions.comments.norevision': 'È necessario creare prima una revisione.',
    'workshop.revisions.comments.select': 'Seleziona revisione',
    'workshop.collaborators.noresults': 'Nessun risultato',
    'workshop.collaborators.add.beta': 'Aggiungi lettore Beta',
    'workshop.collaborators.add.collaborator': 'Aggiungere collaboratore',
    'workshop.collaborators.add.translator': 'Aggiungi traduttore',
    'workshop.collaborators.language.add': 'Crea una nuova traduzione',
    'workshop.translations.stories': 'Traduzioni - storie',
    'workshop.translations.universes': 'Traduzioni - universi',
    'workshop.revisions.beta.none': 'Al momento non ci sono revisioni.',
    'reader.asidetoggle': 'Nascondi informazioni',
    'workshop.translation.original': 'Originale:',
    'workshop.translation.original.title': 'Titolo originale:',
    'workshop.translation.original.description': 'Descrizione originale:',
    'workshop.translation.original.cover': 'Copertina originale',
    'workshop.translation.original.tags': 'Tag originali',
    'workshop.cover.alt': 'immagine di copertina',
    'workshop.translation.updatedOrig': 'Ultimo aggiornamento dell\'originale: {updatedAt, date, long} {updatedAt, time, medium}',
    'workshop.translation.updatedTrs': 'Ultimo aggiornamento della traduzione: {updatedAt, date, long} {updatedAt, time, medium}',
    'workshop.translation.revisionOrig': 'Ultima revisione {revision, number}, dell\'originale: {updatedAt, date, long} {updatedAt, time, medium}',
    'workshop.translation.revisionTrs': 'Ultima revisione {revision, number}, della traduzione: {updatedAt, date, long} {updatedAt, time, medium}',
    'workshop.collaborators.premium': `Per aggiungere più {type, select,
    collaborator {collaboratori}
    beta {lettori beta}
    translator {traduttori}
    other {}
  }, è necessario un livello di iscrizione più alto.`,
    'workshop.collaborators.requests.sent': 'Richieste inviate',
    'workshop.collaborators.requests.invite': `Un invito per te a diventare {role, select,
    beta {lettore beta}
    translator {traduttore}
    collaborator {collaboratore}
    other {}
  } per:`,
    'workshop.collaborators.requests.dashboard': `Un invito per te a collaborare a {count, plural,
    zero {nessun progetto}
    one {un progetto letterario}
    other {# progetti letterari}
  }.`,
    'workshop.collaborators.language.added': 'Lingua aggiunta',
    'workshop.story.noChaptersFound': 'Nessun capitolo trovato.',
    'workshop.fanfic.needUniverseInLibrary': 'Ci sono molti universi là fuori. Aggiungi alla tua biblioteca prima quelli in cui vorresti scrivere in modo da poterli trovare.',
    'workshop.fanfic.findUniverse': 'Trova un universo incredibile!',
    'workshop.encSettings': 'Impostazioni enciclopedia',
    'workshop.collaborators.removeUser': 'Rimuovi utente',
    'workshop.collaborators.cancelInvite': 'Annulla invito',
    'workshop.universe.createFirstStory': 'Crea la prima storia',
    'workshop.storylines': 'Trame',
    'workshop.scenesManager': 'Gestore Scene',
    'workshop.collaborators.addRoleForLang': 'Aggiungi {role} per {language}',
    'workshop.beta.revisionInfo': 'Questa è la revisione {revision, number}, creata il {createdAt, date, long}.',
    'workshop.story.notes.title': 'I tuoi appunti per {storyTitle}',
    'workshop.story.notes.desc': 'Note generali per la storia.',
    'workshop.story.notes.tile': 'Note generali',
    'workshop.hideOrgs': 'Nascondi contenuto alle organizzazioni',
    'workshop.translation.premium': 'Per aggiungere traduzioni, è necessario avere almeno l\'abbonamento Explorer.',
    'workshop.story.finished': 'La storia è completa',
    'workshop.story.publishing.externalSellingNotice': 'Visto che le tue vendite sono esterne a Literary Universe, ora puoi pubblicare la storia. I link dei tuoi negozi appariranno sulla storia oppure alla fine dell\'attuale storia che pubblichi qui.',
    'workshop.sharing.publicURL': 'Versione pubblica',
    'workshop.fictionalTime.title': 'Orari immaginari per {universeName}',
    'workshop.fictionalTime.none': 'Al momento non hai nessun tempo immaginario impostato.',
    'workshop.fictionalTime.createNow': 'Creane uno adesso!',
    'workshop.fictionalTime.createTitle': 'Crea un nuovo tempo immaginario',
    'workshop.fictionalTime.editTitle': 'Modifica {ftName}',
    'workshop.fictionalTime.formDescription': `Qui puoi creare il tuo tempo immaginario. Al momento questa funzione è limitata e può creare solo
        tempi simmetrici. Ciò significa che non potrai ricreare il calendario Maya o quello Gregoriano. Al momento
        per questa funzione tutti i tempi sono sempre gli stessi, es. i mesi hanno sempre 30 giorni, senza eccezioni e così
        via. In futuro aggiorneremo questa funzione, ma per ora possono essere creati
        solo tempi standard di sci-fi.`,
    'workshop.fictionalTime.timeName': 'Nome del tempo',
    'workshop.fictionalTime.basicInfo': 'Informazioni di base',
    'workshop.fictionalTime.timeSetup': 'Configurazione ora',
    'workshop.fictionalTime.linkedToET': 'Questo tempo inizia in una data della Terra (calendario gregoriano)?',
    'workshop.fictionalTime.timeDeclaration': 'Dichiarazione temporale',
    'workshop.fictionalTime.declarationLocation': 'Dove dovrebbe essere dichiarato il nome del tempo?',
    'workshop.fictionalTime.declarationBefore': 'Prima delle unità',
    'workshop.fictionalTime.declarationAfter': 'Dopo le unità',
    'workshop.fictionalTime.declarationNone': 'Nessuna',
    'workshop.fictionalTime.units': 'Unità',
    'workshop.fictionalTime.ETStart': 'Data d\'inizio di questo tempo',
    'workshop.fictionalTime.unitsDescFirst': 'How many units are there in this unit from milliseconds?',
    'workshop.fictionalTime.unitsDesc': 'How many units is this one from the previous one?',
    'workshop.fictionalTime.unitsName': 'Unit name',
    'workshop.fictionalTime.unitsSeparator': 'Separator from previous unit',
    'workshop.fictionalTime.unitAdd': 'Add unit',
    'workshop.fictionalTime.clocks': 'Clocks',
    'workshop.fictionalTime.unitTransfer': 'Transfer units',
    'fictionalTime.pageStartElapsedTime': 'Tempo trascorso dall\'apertura di questa pagina',
    'fictionalTime.currentTime': 'Ora corrente',
    'workshop.shoppingExternal.options': 'Opzioni di acquisto esterne',
    'workshop.shoppingExternal.explainer': 'Se la storia viene venduta altrove, è possibile impostare i link qui. I link verranno visualizzati al posto del contenuto o alla fine del contenuto di cui si sceglie l\'anteprima.',
    'workshop.shoppingExternal.amazonStore': 'Negozio Amazon',
    'workshop.shoppingExternal.amazon': 'Amazon',
    'workshop.shoppingExternal.storeName': 'Nome del negozio',
    'workshop.shoppingExternal.itemUrl': 'URL Articolo',
    'workshop.shoppingExternal.storesOther': 'Altri negozi',
    'fictionalTime.units.milliseconds': '{units, number} milliseconds',
    'fictionalTime.countdownToStart': 'Countdown to time beginning',
    'fictionalTime.unitsToETList': `{units, number} {unitName} = 
    {days, plural,
      =0 {}
      one {1 day }
      other {{days} days }
    }{hours, plural,
      =0 {00:}
      other {{hours}:}
    }{minutes, plural,
      =0 {00:}
      other {{minutes}:}
    }{seconds, plural,
      =0 {00}
      other {{seconds}}
    }
  `,
    'fictionalTime.ETToUnitsList': `{units, number} {unitType, select,
    seconds {{units, plural,
      one {second}
      other {seconds}
    }}
    minutes {{units, plural,
      one {minute}
      other {minutes}
    }}
    hours {{units, plural,
      one {hour}
      other {hours}
    }}
    days {{units, plural,
      one {day}
      other {days}
    }}
    years {{units, plural,
      one {year}
      other {years}
    }}
    other {}
  } = {targetResult, number} {targetType}`,
    'fictionalTime.inputFrom': 'From',
    'fictionalTime.inputSelectTimeUnit': 'Select time unit',
    'fictionalTime.inputSelectETUnit': 'Select Earth time unit',
    'fictionalTime.ETDateToTimeDate': '{date, date, medium} = {timeDate}',
    'workshop.writerSidebar.title': 'Barra Laterale Strumenti',
    'workshop.writerSidebar.description': 'Benvenuti nella vostra storia! Questa barra laterale vi permette di accedere facilmente a tutte le risorse relative alla vostra storia. Controlla le icone in alto per vedere gli ultimi commenti, le tue note e molto altro ancora!',
    'workshop.story.textopian': 'Link to Textopian',
    'workshop.story.textopianExplained': 'Textopian is a reading and discussion platform for works in the public domain.',
    'workshop.story.authorsNotesExplained': 'Le vostre note per i lettori da leggere all\'inizio del capitolo.',
    'workshop.story.chapterSortSwitch': 'Cambiare l\'ordine dei capitoli',
    'workshop.universe.monetizationTitle': '{title} monetization',
    'workshop.story.monetizationTitle': '{universeTitle}: {storyTitle} monetization',
    'workshop.universe.monetizationWebMonetization': 'Web Monetization',
    'workshop.webMonetization.explain': 'Web Monetization will stream payments to you based on how much time user spends on your content. In your profile monetization settings you can set your own payment pointer. Here you can decide on a split with your collaborators who have Web Payment enabled. Literary Universe automatically takes 10%.',
    'workshop.webMonetization.revShare': 'Due to the limitation of Web Monetization probabilistic revenue sharing is used. Probabilistic revenue sharing works by randomly choosing from a list of predefined payment pointers each time a web monetized visitor loads your page. The visitor pays to the chosen pointer until the page is reloaded or closed.',
    'workshop.webMonetization.revShareMore': 'Per saperne di più sulla ripartizione probabilistica dei ricavi. Utilizziamo lo stesso codice di quella pagina.',
    'workshop.story.notEligible.licence': `La licenza della storia non consente la monetizzazione.`,
    'workshop.universe.notEligible.licence': `La licenza dell'universo non consente la monetizzazione.`,
    'workshop.story.notEligible.fanFiction': 'Le storie di Fan Fiction non possono essere monetizzate.',
    'workshop.revShare.mustBe100': 'La quota deve essere pari al 100%',
    'workshop.revShare.LUCut': 'Literary Universe & payment gate',
    'workshop.revShare.unallocated': 'Quota non assegnata {total, number, ::percent}',
    'workshop.revShare.allocation': '{username} - {total, number, ::percent}',
    'workshop.revShare.undecided': 'Indeciso',
    'workshop.revShare.origLangCreators': 'Creatori in lingua originale',
    'workshop.story.prolog': 'Prolog',
    'workshop.story.chapter': 'Capitolo {sequence, number}',
    'workshop.work.includesAds': 'Ci sono pubblicità o inserimenti di prodotti nell\'opera?',
    'workshop.universe.forumSettings': 'Impostazioni del forum dell\'universo interno',
    'workshop.universe.forum': 'Forum interno di {universeName}',
    'workshop.ratingAdjusted': 'Due to complains we had to adjust your rating to the one that we felt appropriate. As such changing the rating is now disabled.',
    'workshop.selectCollaborator': 'Select collaborator',
    'workshop.chapters.scheduledOn': '{publishDate, date, short} | {publishDate, time}',
    'workshop.chapterStatus.draft': 'Draft',
    'workshop.chapterStatus.scheduled': 'Scheduled',
    'workshop.chapterStatus.published': 'Published'
};