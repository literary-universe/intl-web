export default {
    'library.add': 'Aggiungi alla libreria',
    'library.remove': 'Rimuovi dalla libreria',
    'library.bought': 'Storie acquistate',
    'library.stories': 'Storie seguite',
    'library.universes': 'Universo seguite'
};