export default {
    'discovery.meta.desc': 'Scopri nuove storie e universi.',
    'discovery.universes.new': 'Universi emergenti',
    'discovery.stories.new': 'Nuove storie',
    'discovery.stories.standalone.new': 'Nuove storie autonome',
    'discovery.stories.updated': 'Storie aggiornate da poco',
    'discovery.continue': 'Continua a leggere',
    'discovery.stories.similar.new': 'Nuove storie in circolazione',
    'discovery.stories.similar.regular': 'Storie in circolazione',
    'story.estimatedReadingTime': `Questa storia si legge in {hours, plural,
        =0 {}
        one { 1 ora e }
        other {# ore e }
    }{minutes, plural,
        =0 {0 minuti}
        one {1 minuto}
        other {# minuti}
    }.`,
    'story.estimatedReadingTime.explained': 'Secondo una velocità di lettura media di {avgReadingNum} parole al minuto.',
    'discover.search.text': 'Cerca un termine',
    'discover.searchStory.title': 'Cerca una storia',
    'discover.searchStory.description': 'Cerca delle storie',
    'discover.searchUniverse.title': 'Cerca un universo',
    'discover.searchUniverse.description': 'Cerca degli universi',
    'discover.search.storyStatus': 'Stato progressi storia',
    'story.status.complete': 'Completato',
    'story.status.inProgress': 'In corso',
    'discover.search.universeConnection': 'Collegamento storia a universo',
    'story.universeRelation.standalone': 'Storia autonoma',
    'story.universeRelation.fanFiction': 'Fan Fiction',
    'story.universeRelation.universe': 'Parte di un universo',
    'discover.search.rating': 'Classificazione età',
    'discover.search.license': 'Licenza di copyright',
    'discover.search.language': 'Lingua',
    'discover.search.resultsTotal.universe': `{results, plural,
        =0 {Nessun universo corrisponde ai parametri}
        one {1 universo trovato}
        other {# universi trovati}
    }.`,
    'discover.search.resultsTotal.story': `{results, plural,
        =0 {Nessuna storia corrisponde ai parametri}
        one {1 storia trovata}
        other {# storie trovate}
    }.`,
    'discover.viewSelection.aria': 'Mostra selezione',
    'discover.searchDisplay.covers': 'Riguarda l\'elenco',
    'discover.searchDisplay.list': 'Elenco compatto',
    'discover.includesAds': 'Questo lavoro include pubblicità indiretta o altre forme di pubblicità.'
};