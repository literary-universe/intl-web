export default {
  'roadmap.ariaTimeOptions': 'Selezione delle voci della roadmap in base al loro stato.',
  'roadmap.state.backlog': 'Backlog',
  'roadmap.state.inprogress': 'In corso',
  'roadmap.state.preview': 'Anteprima',
  'roadmap.state.beta': 'Beta',
  'roadmap.state.ga': 'Disponibilità generale',
  'roadmap.eta.q1.short': 'Q1',
  'roadmap.eta.q2.short': 'Q2',
  'roadmap.eta.q3.short': 'Q3',
  'roadmap.eta.q4.short': 'Q4',
  'roadmap.eta.q1.long': 'First quarter',
  'roadmap.eta.q2.long': 'Second quarter',
  'roadmap.eta.q3.long': 'Third quarter',
  'roadmap.eta.q4.long': 'Fourth quarter',
  'roadmap.backlog': 'Backlog',
  'roadmap.history': 'Storia dello sviluppo',
  'roadmap.item.inBacklog': 'Abbiamo in programma di sviluppare questa funzione in futuro.',
  'roadmap.item.inProgress': 'The development of the feature started {startedAt, date}.',
  'roadmap.item.inBeta': 'This feature has entered beta on {betaAt, date}.',
  'roadmap.item.inPreview': 'This feature became available to Storyteller subscribers on {previewAt, date}.',
  'roadmap.item.inGa': 'This feature has become generally available on {gaAt, date}.',
  'roadmap.item.originalIssue': 'This feature was originally suggested by our users.',
  'roadmap.eta.mapTitle': `Planned for {quarter, select,
    Q1 {Q1}
    Q2 {Q2}
    Q3 {Q3}
    Q4 {Q4}
    other {}
  } {year, date, ::yyyy}`,
  'roadmap.eta.mapTitleFinished': 'Finished in {time, date, ::MMMM ::yyyy}',
  'roadmap.back': 'Back to roadmap',
  'roadmap.seeBacklog': 'Scopri le altre cose che abbiamo in programma di fare',
  'roadmap.seeFinished': 'Se volete approfondire la nostra storia, abbiamo preparato una bella panoramica per voi',
  'roadmap.planned': 'Pianificato',
  'roadmap.finished': 'Finito'
};