// Notifications & flashnews
export default {
  'flashnews.create': 'Neuen News-Flash erstellen',
  'flashnews.startsAt': 'Starte die Anzeige dieser Nachricht um',
  'flashnews.endsAt': 'Diese Nachricht nicht mehr anzeigen bei',
  'flashnews.newLanguage': 'Neue Sprache zum Hinzufügen auswählen',
  'flashnews.onlyDisplayOn': 'Die Nachrichten werden nur in den unten ausgewählten Sprachen angezeigt (falls vorhanden), in anderen Sprachen werden keine Nachrichten angezeigt, auch nicht in der Standardsprache.'
};