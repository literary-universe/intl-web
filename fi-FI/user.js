export default {
    'user.friend.request.add': 'Lisää ystäville',
    'user.friend.requests': `You have {num, number} new friend {num, plural,
    zero {requests}
    one {request}
    many {requests}
    other {requests}
  }`,
    'user.friend.unfriend': 'Unfriend',
    'user.block': 'Block',
    'user.unblock': 'Unblock',
    'user.joined': `{gender, select,
    male {Joined}
    female {Joined}
    other {Joined}} on {date, date, long}`,
    'user.friend.request.accept': 'Accept friendship request',
    'user.friend.request.cancel': 'Cancel friendship request',
    'user.friend.request.deny': 'Deny friendship',
    'user.listing': 'User listing',
    'user.profile.visit': 'Visit {user}\'s profile.',
    'user.profile.avatar': '{username}\'s avatar',
    'user.friend.list.requests': 'Ystävyyspyynnöt',
    'user.friend.norequests': 'You have no requests for friendship.',
    'user.dashboardWelcome': `Good {dayPart, select,
    morning {morning}
    afternoon {afternoon}
    evening {evening}
    night {night}
    other {day}
  } {displayName}!`,
    'user.verification.publishedAuthor': 'Tämä kirjailija on julkaistu perinteisellä tavalla',
    'user.verification.luAuthor': 'Literary Universe based author of note',
    'user.verification.luEmployee': 'Literary Universe employee',
    'user.verification.publicFigure': 'Todennettu julkisuuden henkilö'
};