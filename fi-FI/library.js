export default {
    'library.add': 'Lisää kirjastoon',
    'library.remove': 'Poista kirjastosta',
    'library.bought': 'Ostetut tarinat',
    'library.stories': 'Seuratut tarinat',
    'library.universes': 'Followed universes'
};