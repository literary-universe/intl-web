// Notifications & flashnews
export default {
  'flashnews.create': 'Create a new news flash',
  'flashnews.startsAt': 'Start displaying this message at',
  'flashnews.endsAt': 'Stop displaying this message at',
  'flashnews.newLanguage': 'Select new language to add',
  'flashnews.onlyDisplayOn': 'The news is going to be displayed only on the languages selected bellow (if any selected), there will be no news shown, even in the default language, in other languages.'
};