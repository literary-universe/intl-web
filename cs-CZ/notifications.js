// Notifications & flashnews
export default {
  'flashnews.create': 'Vytvořit novou rychlou novinku',
  'flashnews.startsAt': 'Začněme zobrazovat tuto zprávu od',
  'flashnews.endsAt': 'Přestaneme zobrazovat tuto zprávu od',
  'flashnews.newLanguage': 'Vyberte nový jazyk pro přidání',
  'flashnews.onlyDisplayOn': 'Zprávy budou zobrazeny pouze na jazycích vybraných níže (pokud jsou vybrány). Zpráva nebude zobrazena ve výchozím jazyku pro uživatele s jiným jazykem.'
};