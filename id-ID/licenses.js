/**
 * Licenses for creative works
 * https://creativecommons.org/share-your-work/licensing-types-examples/licensing-examples/
 */
export default {
    'licenses.about': 'Tentang Lisensi',
    'licenses.more': 'Informasi lebih lanjut',
    'licenses.legal': 'Dokumen hukum',
    'licenses.name.cc-by': 'Creative Commons Attribution 4.0',
    'licenses.cc-by': 'Lisensi ini mengizinkan orang lain untuk mengubah, memperbaiki, membuat ciptaan turunan, dan mendistribusikan secara komersial, selama mereka mencantumkan kredit kepada Anda atas ciptaan asli. Lisensi ini merupakan lisensi yang paling bebas. Lisensi ini direkomendasikan untuk pendistribusian luas secara maksimal dan penggunaan materi berlisensi.',
    'licenses.name.cc-by-sa': 'Creative Commons Attribution-ShareAlike 4.0',
    'licenses.cc-by-sa': 'This license lets others remix, tweak, and build upon your work even for commercial purposes, as long as they credit you and license their new creations under the identical terms. This license is often compared to “copyleft” free and open source software licenses. All new works based on yours will carry the same license, so any derivatives will also allow commercial use. This is the license used by Wikipedia, and is recommended for materials that would benefit from incorporating content from Wikipedia and similarly licensed projects.',
    'licenses.name.cc-by-nd': 'Creative Commons Attribution-NoDerivs 4.0',
    'licenses.cc-by-nd': 'This license allows for redistribution, commercial and non-commercial, as long as it is passed along unchanged and in whole, with credit to you.',
    'licenses.name.cc-by-nc': 'Creative Commons Attribution-NonCommercial 4.0',
    'licenses.cc-by-nc': 'This license lets others remix, tweak, and build upon your work non-commercially, and although their new works must also acknowledge you and be non-commercial, they don’t have to license their derivative works on the same terms.',
    'licenses.name.cc-by-nc-sa': 'Creative Commons Attribution-NonCommercial-ShareAlike 4.0',
    'licenses.cc-by-nc-sa': 'This license lets others remix, tweak, and build upon your work non-commercially, as long as they credit you and license their new creations under the identical terms.',
    'licenses.name.cc-by-nc-nd': 'Creative Commons Attribution-NonCommercial-NoDerivs 4.0',
    'licenses.cc-by-nc-nd': 'This license is the most restrictive of our six main licenses, only allowing others to download your works and share them with others as long as they credit you, but they can’t change them in any way or use them commercially.',
    'licenses.name.lu-c': 'Hak Cipta Literary Universe',
    'licenses.lu-c': 'Standard full copyrights license that allows you to sell publish your work as you like and have your rights to it protected. This license also gives you the option to sell your stories on Literary Universe.',
    'licenses.name.cc0': 'No Rights Reserved - CC0 1.0',
    'licenses.cc0': 'Put your work into public domain. In contrast to CC’s licenses that allow copyright holders to choose from a range of permissions while retaining their copyright, CC0 empowers yet another choice altogether – the choice to opt out of copyright and database protection, and the exclusive rights automatically granted to creators – the “no rights reserved” alternative to our licenses.'
};