export default {
    'legal.modal.intro': 'Desculpe interromper. Mas antes de deixá-lo ir mais longe, estamos legalmente obrigados a garantir que você esteja ciente e de acordo com nossos Termos de Serviço, Política de Privacidade e Acordo de Direitos Autorais.',
    'legal.modal.agree': 'Eu concordo',
    'legal.modal.disagree': 'Não Concordo',
    'legal.modal.register': 'Você não verá mais isto se você se registrar.',
    'legal.modal.goToSettings': 'Vá para suas configurações',
    'legal.modal.fewmore': 'Poucas coisas mais...',
    'legal.modal.finish': 'Concluir'
};