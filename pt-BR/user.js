export default {
    'user.friend.request.add': 'Adicionar aos amigos',
    'user.friend.requests': `You have {num, number} new friend {num, plural,
    zero {requests}
    one {request}
    many {requests}
    other {requests}
  }`,
    'user.friend.unfriend': 'Desfazer Amizade',
    'user.block': 'Bloquear',
    'user.unblock': 'Desbloquear',
    'user.joined': `{gender, select,
    male {Juntou-se a}
    female {Juntou-se a}
    other {Juntou-se a}} dia {date, date, long}`,
    'user.friend.request.accept': 'Aceitar pedido de amizade',
    'user.friend.request.cancel': 'Cancelar pedido de amizade',
    'user.friend.request.deny': 'Negar amizade',
    'user.listing': 'Listagem de usuários',
    'user.profile.visit': 'Visite o perfil de {user}.',
    'user.profile.avatar': 'Avatar de {username}',
    'user.friend.list.requests': 'Pedidos de amizades',
    'user.friend.norequests': 'Você não tem nenhum pedido de amizade.',
    'user.dashboardWelcome': `Bom {dayPart, select,
    morning {manhã}
    afternoon {tarde}
    evening {noite}
    night {dia}
    other {dia}
  } {displayName}!`,
    'user.verification.publishedAuthor': 'Este autor tem sido publicado da maneira tradicional',
    'user.verification.luAuthor': 'Autor de nota com base no Universo Literário',
    'user.verification.luEmployee': 'Funcionário do Universo Literário',
    'user.verification.publicFigure': 'Figura pública verificada'
};