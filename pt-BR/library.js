export default {
    'library.add': 'Adicionar à biblioteca',
    'library.remove': 'Remover da biblioteca',
    'library.bought': 'Histórias compradas',
    'library.stories': 'Histórias seguidas',
    'library.universes': 'Followed universes'
};