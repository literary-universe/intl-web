export default {
    'discovery.meta.desc': 'Descubra novas histórias e universos.',
    'discovery.universes.new': 'Universos emergentes',
    'discovery.stories.new': 'Novas histórias',
    'discovery.stories.standalone.new': 'Novas histórias autônomas',
    'discovery.stories.updated': 'Histórias atualizadas recentemente',
    'discovery.continue': 'Continue lendo',
    'discovery.stories.similar.new': 'Novas histórias no bairro',
    'discovery.stories.similar.regular': 'Histórias no bairro',
    'story.estimatedReadingTime': `Esta história deve levar {hours, plural,
        =0 {}
        one {1 hora e }
        other {# horas e }
    }{minutes, plural,
        =0 {0 minutos}
        one {1 minuto}
        other {# minutos}
    }.`,
    'story.estimatedReadingTime.explained': 'Baseado em uma velocidade média de leitura de {avgReadingNum} palavras por minuto.',
    'discover.search.text': 'Pesquisar termo',
    'discover.searchStory.title': 'Busca de uma história',
    'discover.searchStory.description': 'Busca de uma história',
    'discover.searchUniverse.title': 'Busca de um universo',
    'discover.searchUniverse.description': 'Busca de um universo',
    'discover.search.storyStatus': 'Status do progresso da história',
    'story.status.complete': 'Completo',
    'story.status.inProgress': 'Em progresso',
    'discover.search.universeConnection': 'Conexão da história com o universo',
    'story.universeRelation.standalone': 'História autônomas',
    'story.universeRelation.fanFiction': 'Ficção do ventilador',
    'story.universeRelation.universe': 'Parte de um universo',
    'discover.search.rating': 'Classificação etária',
    'discover.search.license': 'Licença de direitos autorais',
    'discover.search.language': 'Idioma',
    'discover.search.resultsTotal.universe': `{results, plural,
        =0 {Nenhum universo corresponde a seus parâmetros}
        one {1 universo encontrado}
        other {# universos encontrados}
    }.`,
    'discover.search.resultsTotal.story': `{results, plural,
        =0 {Nenhuma história corresponde aos seus parâmetros}
        one {1 história encontrada}
        other {# histórias encontradas}
    }.`,
    'discover.viewSelection.aria': 'Ver seleção',
    'discover.searchDisplay.covers': 'Listagem de coberturas',
    'discover.searchDisplay.list': 'Lista compacta',
    'discover.includesAds': 'Este trabalho tem colocação de produtos ou inclui outras formas de anúncios.'
};