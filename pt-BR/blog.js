export default {
    'blog.noneMsg': `You currently don't have {type, select,
    organization {any blog}
    universe {a blog for your universe}
    user {a blog}
    other {}
  }.`,
    'blog.create': 'Criar um novo blog',
    'blog.settings': `{type, select,
    org {Blog}
    universe {Universe page}
    user {Blog}
    other {}
  } settings`,
    'blog.description': 'Introdução ao Blog',
    'blog.theme': 'Tema',
    'blog.theme.info': 'Ajustando o tema você será capaz de mudar o visual da página. Este recurso não está pronto neste momento.',
    'blog.posts.total': `There {total, plural,
    zero {are no posts}
    one {is one post}
    other {are # posts}
  } in this blog.`,
    'blog.post.create': 'Criar um novo posto',
    'blog.post.update': 'Editing {title}',
    'blog.post.text': 'Post text',
    'common.slug': 'Endereço amigável de SEO',
    'blog.slug.explained': 'Belo visual url para o posto. Evite usar caracteres especiais, exceto "-".',
    'blog.publicView': 'Opinião pública',
    'blog.post.notfound': 'Não foi encontrado um post no blog',
    'blog.lists.works': `{type, select,
    universes {Universes}
    stories {Standalone stories}
    fanfiction {Fan Fiction work}
    other {}
  }`,
    'blog.lists.stories.disclaimer': 'Listadas aqui estão histórias autônomas, se a história foi escrita como parte de um universo, então ela será listada na página do universo.',
    'blog.social.sameAsMain': `Social links are the same as set in {type, select,
    organization {organization settings}
    user {user profile}
    other {}
  }`,
    'blog.social.website': 'Official website',
    'blog.settings.universeSettings': 'Os blogs do universo são integrados em páginas do universo com suas configurações básicas retiradas de configurações do universo.',
    'blog.settings.organizationSettings': 'Organization blogs are integrated into organization pages.',
    'blogs.lu': 'Official Literary Universe blogs',
    'blogs.featured': 'Featured blogs',
    'blogs.new': 'Newest blogs',
    'blogs.title': 'Blogs of Literary Universe',
    'blogs.description': 'Listing of blogs on the Literary Universe platform.',
    'blogs.visit': 'Visit blog',
    'blog.settingsAria': 'Settings categories for the blog',
    'blog.settings.blog': 'Blog',
    'blog.settings.forum': 'Forum'
};