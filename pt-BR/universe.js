export default {
    'universe.authors.title.meta': '{title} autores',
    'universe.fanfiction.see': 'All Fan Fiction for {universe}',
    'universe.fanfiction.desc': 'Fan Fiction for {universe}',
    'universe.fanfiction.for': '{universe} Fan Fiction',
    'universe.fanarts.for': '{universe} Fan Arts',
    'universe.fansettings.for': '{universe} Fan Settings',
    'universe.fanfiction.stories': 'Stories',
    'universe.fanfiction.fanarts': 'Fan Arts',
    'universe.stories.count': `Oba {num, plural,
    =0 {não há histórias}
    one {é uma história}
    other {são # histórias}
    } neste universo.`,
    'universe.authors.title': 'Autores',
    'universe.authors.desc': 'Criadores e colaboradores de {universe}',
    'universe.contributors': 'Colaboradores',
    'universe.translators': 'Tradutores',
    'universe.authors.notice': 'Cada história pode ter pessoal adicional que tenha trabalhado nela. Para mais detalhes, veja os detalhes da história.',
    'universe.createdBy': `{gender, select,
    male {Created}
    female {Created}
    other {Created}} by `,
    'universe.stories.for': 'Histórias de {universe}',
    'universe.stories.own': '{universe} histórias',
    'universe.stories.desc': 'Cannon stories for {universe}',
    'universe.encyclopedia.title': 'Enciclopédia {universe}',
    'universe.encyclopedia.desc': 'All the details for {universe}',
    'universe.fanArt.submit': 'Submit your art',
    'universe.fanArtHeaderDesc.item': '{universe} fan art - {name}',
    'universe.fanArtHeaderDesc.submit': '{universe} fan art submission',
    'universe.fanArtHeaderDesc.overview': '{universe} fan art'
};