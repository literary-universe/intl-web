export default {
    'encyclopedia.add': 'Add a new entry',
    'encyclopedia.browse': 'Navegue pela enciclopédia',
    'encyclopedia.search': 'Procure na enciclopédia',
    'encyclopedia.back.universe': 'Back to universe page',
    'encyclopedia.back.workshop': 'Back to workshop',
    'encyclopedia.categories': 'Categorias',
    'encyclopedia.groups': 'Grupos',
    'encyclopedia.category.character': 'Caracteres',
    'encyclopedia.category.event': 'Eventos',
    'encyclopedia.category.group': 'Grupos',
    'encyclopedia.category.item': 'Itens',
    'encyclopedia.category.location': 'Localização',
    'encyclopedia.category.other': 'Outros',
    'encyclopedia.category.state': 'Estados',
    'encyclopedia.category.governments': 'Governo',
    'encyclopedia.category.languages': 'Idiomas',
    'encyclopedia.category.creatures': 'Criaturas',
    'encyclopedia.category.deity': 'Divindades',
    'encyclopedia.category.religions': 'Religiões',
    'encyclopedia.category.flora': 'Flore',
    'encyclopedia.category.magic': 'Mágica',
    'encyclopedia.category.planets': 'Planetas',
    'encyclopedia.category.species': 'Espécies',
    'encyclopedia.category.technologies': 'Tecnologias',
    'encyclopedia.search.found': `{results, plural,
    =0 {No related entries}
    one {1 related entry}
    other {# related entries}
  } found.`,
    'encyclopedia.help.text': 'Use o menu à esquerda para navegar entre os itens na enciclopédia. Depois de selecionar um iten, será exibido no lugar deste texto.',
    'encyclopedia.help.sidebar': 'Sidebar Toolbox',
    'encyclopedia.help.sidebar.text': 'Welcome to your story! This sidebar allows you easy access to all resources related to your story. Check the icons above to see latest comments, your notes, search your encyclopedia, browse and add new entries to your encyclopedia. If you want to add connection to an entry from the story text, select the text where you want to place it and then click the connect button. You will be then prompted to find and select an item.',
    'encyclopedia.intro': 'Welcome to the {universe} encyclopedia.',
    'encyclopedia.add.title': 'Add a new entry to encyclopedia',
    'encyclopedia.add.heading': 'O que você deseja adicionar?',
    'encyclopedia.add.moresoon': 'More categories coming soon!',
    'encyclopedia.create': `Create a new {category, select,
    character {character}
    event {event}
    group {group}
    item {item}
    location {location}
    other {note}
    state {state}
    governments {government}
    languages {language}
    creatures {creature}
    deity {deity}
    religions {religion}
    flora {flora}
    magic {magic}
    planets {planet}
    species {specie}
    technologies {technology}
  }`,
    'encyclopedia.create.header': `Create a new {category, select,
    character {character}
    event {event}
    group {group}
    item {item}
    location {location}
    other {note}
    state {state}
    governments {government}
    languages {language}
    creatures {creature}
    deity {deity}
    religions {religion}
    flora {flora}
    magic {magic}
    planets {planet}
    species {specie}
    technologies {technology}
  } in {universe}`,
    'encyclopedia.entry.category': `Entry category: {category, select,
    character {Character}
    event {Event}
    group {Group}
    item {Item}
    location {Location}
    other {Note}
    state {State}
    governments {Government}
    languages {Language}
    creatures {Creature}
    deity {Deity}
    religions {Religion}
    flora {Flora}
    magic {Magic}
    planets {Planet}
    species {Specie}
    technologies {Technology}
  }`,
    'encyclopedia.entry.category.legend': 'Categoria',
    'encyclopedia.fields.add': 'Adicionar um novo campo',
    'encyclopedia.fields.text': 'Definição',
    'encyclopedia.fields.textarea': 'Texto',
    'encyclopedia.fields.connection': 'Conexão',
    'encyclopedia.fields.time': 'Time',
    'encyclopedia.fields.image': 'Imagem',
    'encyclopedia.visibility': 'Visibilidade',
    'encyclopedia.public': 'Público',
    'encyclopedia.private': 'Privado',
    'encyclopedia.container': 'Contêiner',
    'encyclopedia.container.desc': 'Make this entry a container for a new group of entries. This will hide it from categories listing.',
    'encyclopedia.entry.name': 'Name of the entry',
    'encyclopedia.summary': 'Sumário',
    'encyclopedia.summary.desc': 'This is a summary that will be the only initial information displayed on the reader.',
    'encyclopedia.parent': 'Parent entry',
    'encyclopedia.related': 'Related entries',
    'encyclopedia.errors.tags': 'You can only have 10 tags per entry.',
    'encyclopedia.field.private': 'Campo privado',
    'encyclopedia.ft.indev': 'Esta opção está em desenvolvimento.',
    'encyclopedia.connections.remove': 'Remover conexão',
    'encyclopedia.connections.extra': 'Informações extras',
    'encyclopedia.connections.existing': 'Existing entry',
    'encyclopedia.connections.existing.abbr': 'EE',
    // abbreviation for Existing Entry
    'encyclopedia.cover': 'Cover image',
    'encyclopedia.translation.title': 'Welcome to {universe} universe translation!',
    'encyclopedia.translation.desc': 'This encyclopedia interface is used for translation into {language}. Choose existing entries in the list on the left to begin their translation.',
    'encyclopedia.welcome.totalEntries': `There {totalEntries, plural, 
    zero {are no entries}
    one {is one entry}
    other {are # entries}
  } in this universe.`,
    'encyclopedia.settings.title': 'Encyclopedia settings',
    'encyclopedia.settings.categoriesDesc': 'Turn on and off categories which you want to use in your encyclopedia.',
    'encyclopedia.settings.introText': 'Welcome message to be displayed after universe description.',
    'encyclopedia.fields.placeholder': 'Field name',
    'encyclopedia.fields.delete': 'Delete field',
    'encyclopedia.timeOption.fictionalDate': 'Fictional date',
    'encyclopedia.timeOption.fictionalTime': 'Fictional time',
    'encyclopedia.timeOption.fictionalDateTime': 'Fictional date time',
    'encyclopedia.timeOption.date': 'Date',
    'encyclopedia.timeOption.time': 'Time',
    'encyclopedia.timeOption.dateTime': 'Date and time',
    'encyclopedia.settings.groupByCategories': 'Include categories in group listing',
    'encyclopedia.menuOpen': 'Abrir menu de Enciclopédia',
    'encyclopedia.menuClose': 'Fechar menu da Enciclopédia',
    'encyclopedia.menuAria': 'Menu de Enciclopédia',
    'enc.newField': 'Novo campo',
    'enc.entries.limit': `You currently have {value, number} out of {max, plural,
    =0 {unlimited}
    other {{max, number}}
  } entries.`,
    'enc.entries.limitReached': 'You have reached maximum allowed encyclopedia entries for you current subscription.'
};