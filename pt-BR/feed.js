export default {
    'feed.name': 'Notícias',
    'feed.post.new': 'Novos postos',
    'feed.post.like': 'Como o correio',
    'feed.post.unlike': 'Ao contrário do correio',
    'feed.post.send': 'Posto',
    'feed.empty': 'Nenhuma entrada na ração.'
};