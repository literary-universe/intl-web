// Notifications & flashnews
export default {
  'flashnews.create': 'Vytvorenie novej bleskové správy',
  'flashnews.startsAt': 'Začnite zobrazovať túto správu od',
  'flashnews.endsAt': 'Prestaňte zobrazovať túto správu od',
  'flashnews.newLanguage': 'Vyberte nový jazyk, ktorý chcete pridať',
  'flashnews.onlyDisplayOn': 'Správy sa budú zobrazovať len v jazykoch vybraných nižšie (ak sú vybrané), v iných jazykoch sa nebudú zobrazovať žiadne správy, dokonca ani v predvolenom jazyku.'
};