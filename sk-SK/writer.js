export default {
    'writer.menu.optionsAria': 'Menu možností editora',
    'writer.menu.saveOptionsAria': 'Možnosti ukladania',
    'writer.undo': 'Odvolať',
    'writer.redo': 'Vpred',
    'writer.ul': 'Neusporiadaný zoznam',
    'writer.ol': 'Objednaný zoznam',
    'writer.blockquote': 'Citácia',
    'writer.link': 'Vloženie odkazu',
    'writer.image': 'Vložiť obrázok',
    'writer.bold': 'Výber tučným písmom',
    'writer.underline': 'Podčiarknite výber',
    'writer.italic': 'Zdôraznite výber',
    'writer.wordcount': '{characters, number} znakov, {words, number} slov',
    'finder.details': 'podrobnosti',
    'finder.remove': 'odstrániť spojenie',
    'finder.connect': 'Pripojenie výberu k záznamu v encyklopédii',
    'writer.aside.ariaLabel': 'Menu postráního panelu',
    'writer.nothingToSave': 'Nie je čo uložiť',
    'writer.publish.publishAria': 'Publication options',
    'writer.publish.scheduleExplained': 'You can either publish immediately or schedule the chapter to be available at a later date.',
    'writer.publish.schedule': 'Set publication date time',
    'writer.publish.previousChapterPublished': 'The previous chapter was published on {publishDate, date, short} at {publishDate, time}',
    'writer.publish.previousChapterScheduled': 'The previous chapter is scheduled to be published on {publishDate, date, short} at {publishDate, time}'
};