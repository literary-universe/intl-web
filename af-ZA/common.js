/**
 * @authors Jan Dvorak
 */
/* eslint-disable max-len */
export default {
    siteName: 'Literary Universe',
    twitterHandle: '@lituniapp',
    'site.description': 'Literary Universe is an all-in-one creative and publishing platform for grand story universes.',
    'site.keywords': 'creative writing, scifi, sci-fi, fantasy, writing, publishing, story, stories, universe, web novel, webnovel',
    'site.logoAlt': 'Literary Universe logo',
    mobileMenu: 'Mobile menu',
    'common.signin': 'Teken Aan',
    'common.signup': 'Registreer',
    'common.signout': 'Teken Uit',
    'common.dashboard': 'Dashboard',
    'common.discovery': 'Discovery',
    'common.library': 'Biblioteek',
    'common.library.own': 'Jou Biblioteek',
    'common.workshop': 'Werkswinkel',
    'common.pm': 'Boodskappe',
    'common.settings': 'Instellings',
    'common.options': 'Opsies',
    'common.profile': 'Profiel',
    'common.blog': 'Blog',
    'common.blogs': 'Blogs',
    'common.about': 'Oor Ons',
    'common.accessdenied': 'Toegang verbied',
    'common.accessdenied.msg': 'Jy het nie toegang om hier te wees nie. As jy veronderstel is om toegang te hê, kontak asseblief die eienaar en vra hulle om na die instellings te kyk.',
    'common.loading': 'Laai tans...',
    'common.add': 'Voeg by',
    'common.save': 'Stoor',
    'common.email': 'E-pos',
    'common.soon': 'Binnekort beskikbaar...',
    'common.search': 'Soek',
    'common.search.do': 'Soek',
    'common.searching': 'Soek...',
    'common.report': 'Rapporteer',
    'common.user': 'Gebruiker',
    'common.users': 'Gebruikers',
    'common.showmore': 'Wys meer',
    'common.futurefeature.title': 'Toekomstige funksie',
    'common.futurefeature.text': 'Ons is baie jammer, maar hierdie funksie is nie op die oomblik beskikbaar nie. Ons is hard aan die werk om dit so gou moontlik beskikbaar te stel.',
    'common.nothingfound': 'Niks gevind',
    'common.explore': 'Verken',
    'common.by': 'deur ',
    // keep the space at the end as username follows TODO: needs fixing
    'common.scratchpad': 'Krapblad',
    'common.scratchpad.own': 'Jou krapblad',
    'common.news': 'Nuus',
    'common.encyclopedia': 'Ensiklopedie',
    'common.art': 'Art',
    'common.story': 'Storie',
    'common.stories': 'Stories',
    'common.universe': 'Heelal',
    'common.universes': 'Heelalle',
    'common.collaborator': 'Medewerker',
    'common.collaborators': 'Medewerkers',
    'common.translator': 'Vertaler',
    'common.translators': 'Vertalers',
    'common.betareader': 'Proefleser',
    'common.betareaders': 'Proeflesers',
    'common.post': 'Plasing',
    'common.statistics': 'Statistieke',
    'common.lang.select': 'Kies Taal',
    'common.submit': 'Dien In',
    'common.close': 'Maak toe',
    'common.comment': 'Kommentaar',
    'common.comments': 'Kommentaar',
    404: 'Bladsy bestaan nie',
    'search.users': 'Soek gebruikers',
    'search.user': 'Soek gebruiker',
    'common.help': 'Hulp',
    'common.or': 'of',
    'common.account.create': 'Skep \'n rekening',
    'signup.username': 'Gebruikersnaam',
    'signup.email': 'Jou e-pos',
    'signup.password.label': 'Jou wagwoord',
    'signup.password.repeat': 'Herhaal wagwoord',
    'common.password': 'Wagwoord',
    'signin.password.forgot': 'Wagwoord vergeet?',
    'dashboard.whatsnew': 'Wat\'s nuut',
    'dashboard.blog': 'Jou blog',
    'common.footer.tagline': 'Dit is maklik om komplekse heelalle te skryf en te lees.',
    'common.footer.plans': 'Planne & Pryse',
    'common.legal.tos': 'Terme van diens',
    'common.legal.community': 'Gemeenskapsriglyne',
    'common.legal.privacy': 'Privaatheidsbeleid',
    'common.legal.copyright': 'Kopiereg ooreenkoms',
    'common.legal.newVersionAvailable': 'A new version ({newVersion}) of this document will come into effect on {effectiveAt, date}.',
    'common.legal.disclaimer': '© {year} Alle regte voorbehou, alle tekste behoort aan hul onderskeie outeurs.',
    'premium.only.title': 'Nie hoë genoeg litmaatskap',
    'premium.only.msg': 'Ons vra om verskoning, u lidmaatskap is nie hoog genoeg om toegang tot hierdie funksie te kry nie.',
    'premium.only.join': 'Ons bied \'n verskeidenheid planne aan sodat almal die meeste uit Literary Universe kan put.',
    'premium.only.link': 'Kyk hierna!',
    'universe.visit': 'Besoek die heelal blad',
    'common.fanmanager': 'Fan manager',
    'common.fanarts': 'Fan arts',
    'common.fanfiction': 'Fan fiction',
    'common.fansettings': 'Fan settings',
    'story.chapter': 'Hoofstuk',
    'story.chapters': 'Hoofstukke',
    'story.genre': 'Genre',
    'story.genres': 'Genres',
    'story.read': 'Lees',
    'story.lastUpdate': 'Last updated on {date, date, medium} {date, time, short}.',
    'story.finishedAt': 'Finished on {date, date, medium}.',
    'story.comments.for': 'Comments for {story} {chapter}.',
    'story.cover.alt': 'Story cover image for {story}.',
    'story.note.fanfiction': 'NOTE: This story is a fan fiction on {universe} universe.',
    copyrights: 'Kopiereg',
    'common.challenges': 'Challenges',
    // for beta only
    'beta.notice': 'Welcome to Literary Universe BETA. This is a public preview and as such the app is still work in progress. This means that you can still expect errors and unfinished features. If you encounter an error or something not working please let us know and please be patient while we get get everything in place.',
    'beta.thanks': 'Thank you for your support!',
    'scratchpad.about': 'Your notes and ideas accessible anywhere on the site.',
    'common.feedback': 'Feedback',
    'common.createdOn': 'Created on {date, date, long}',
    'common.createdOn.title': 'Created on',
    // account types
    admin: 'Admin',
    goodbye: 'Thank you! See you again!',
    'common.saved': 'Saved!',
    'common.saving': 'Saving...',
    'common.friends': 'Friends',
    'lang.orig': 'Original language',
    'lang.orig.plus': 'Original language - {language}',
    buy: 'Koop',
    confirm: 'Bevestig',
    cancel: 'Kanselleer',
    processing: 'Processing...',
    'common.noData': 'No data',
    'error.title': 'An error has occurred. We apologize for the inconvenience.',
    'common.offlineStatus': 'You are currently offline.',
    male: 'Manlik',
    female: 'Vroulik',
    'common.legal.effectiveAt': 'Effective from {date, date, medium}',
    select: 'Please select',
    'common.statusFailed': 'Failed to connect to the server.',
    'common.statusConnecting': 'Attempting to connect to the server... ({count, number})',
    'slug.checking': 'Checking...',
    'slug.failed': 'This address already exists.',
    'slug.success': 'This address is available!',
    'notVerified.msg': `Your account {option, select,
    email {e-mail}
    wallet {wallet}
    other {e-mail}
    } is not verified.`,
    'notVerified.verify': `Please verify your {option, select,
    email {e-mail}
    wallet {wallet}
    other {e-mail}
    }.`,
    unknown: 'unknown',
    'common.legal.guidelines': 'Community Guidelines',
    'errorLoading.title': 'Error during page load',
    'errorLoading.explained': 'An error has occurred while the page was loading. We apologize for the inconvenience. Please retry accessing the page again. If the problem persists, please let us known.',
    'errorLoading.retry': 'Probeer weer!',
    'errorLoading.takingLong': 'This is taking a long time...',
    'common.title': 'Titel',
    'common.timelines': 'Timelines',
    'common.legal.gdpr': 'GDPR',
    'common.legal.gdpr.long': 'General Data Protection Regulation',
    'editor.placeholder.link': 'Enter URL...',
    'editor.placeholder.connection': 'Search for encyclopedia entry...',
    'editor.saved.beta': 'New revision for Beta readers created!',
    'editor.saved.public': 'New version published!',
    goodbyeForever: 'We are sorry to see see you go. Was it something we did? Please let us know if that is the case. Either way we wish you the best in your future endeavors and hopefully we\'ll meet again! Live long and prosper!',
    'goodbyeForever.feedback': 'Let us know what we can do better',
    'common.edit': 'Pas aan',
    'common.publish': 'Publish',
    'common.publishAt': 'Publication date and time',
    'a11y.user.menu': 'User menu',
    'a11y.navigation': 'Navigation',
    'dashboard.profile': 'Your profile',
    'profile.website': 'My personal website',
    'story.notes.concept': 'Concept',
    'story.notes.settings': 'Story setting',
    'story.notes.theme': 'Theme',
    'story.notes.impact': 'Desired impact',
    'story.notes.outcome': 'Outcome',
    'story.notes.audience': 'Target audience',
    'story.notes.pitch': 'Pitch',
    'story.notes.notes': 'Notes',
    'story.authorsNotes': 'Author\'s notes',
    'common.members': 'Members',
    'common.language': 'Taal',
    next: 'Volgende',
    previous: 'Previous',
    'common.legal.tos.short': 'Terms',
    'common.legal.privacy.short': 'Privacy',
    'premium.increaseLimit': 'Consider upgrading to increase you limit.',
    'common.sharing': 'Meedeling',
    'story.isbn': 'ISBN',
    'common.name': 'Naam',
    'common.remove': 'Verwyder',
    'common.time.milliseconds': 'Millisekondes',
    'common.time.seconds': 'Sekondes',
    'common.time.minutes': 'Minute',
    'common.time.hours': 'Ure',
    'common.time.days': 'Dae',
    'common.time.weeks': 'Weke',
    'common.time.months': 'Maande',
    'common.time.years': 'Jare',
    'common.monetization': 'Monetization',
    'common.tab.monetizationOptions': 'Monetization options',
    'common.pagination': 'Pagination',
    'common.forum': 'Forum',
    'common.2faCode': '2FA code',
    'common.2faCodeIntro': 'Please enter your one time code generated by your authenticator application.',
    'common.2faCodeAria': '2FA code entry form',
    'common.flashnews': 'Flashnews',
    'common.defaultLanguage': 'Default language',
    'signin.web3.signMessage': 'Please confirm that you want to enter Literary Universe with your wallet as authentication method. Code: {code}',
    'common.web3.confirmationCode': 'Confirmation code: {code}',
    'common.roadmap': 'Roadmap',
    'common.luStatsIFrameOptOut': 'Literary Universe statistics opt-out',
    'common.image': 'Image',
    'common.home': 'Home',
    'common.administration': 'Administration',
    'common.truncateAppend': '...',
    // What should be the symbol used to denote that the text continues when it was truncated
    'common.groups': 'Groups',
    'common.upload': 'Upload',
    'common.backToDashboard': 'Back to dashboard',
    'common.accept': 'Accept',
    'common.reject': 'Reject',
    'common.events': 'Events',
    'common.event': 'Event',
    'common.all': 'All',
    'common.social': 'Social',
    'common.discordCommunity': 'Discord Community',
    'common.account': 'Account',
    'common.notifications': 'Notifications'
}    /* eslint-enable max-len */;