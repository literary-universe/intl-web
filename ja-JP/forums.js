export default {
  'forums.create': '掲示板を有効化',
  'forums.archive': '掲示板をアーカイブ',
  'forums.archive.explained': '掲示板をアーカイブすると、ユーザーはそれ以上の内容を投稿できなくなりますが、閲覧は可能なままです。',
  'forums.archive.success': '掲示板はアーカイブされています',
  'forums.unArchive': '掲示板を再活性化する',
  'forums.unArchive.success': '掲示板が再活性化されました',
  'forums.settings': '掲示板の設定',
  'forums.category.create': '掲示板カテゴリの作成',
  'forums.category.delete': 'カテゴリーを削除する',
  'forums.category.name': 'カテゴリー名',
  'forums.category.slug': 'SEO対策用URL部分',
  'forums.category.settings': 'カテゴリー設定',
  'forums.category.language': 'このカテゴリの表示を次の言語に限定する。すべての言語でカテゴリを表示したい場合は、空のままにします。',
  'forums.category.intlNameDescription': '一般的なカテゴリ名のタイトルをローカライズして選択できるようにし、対応するすべての言語でカテゴリを利用できるようにしました。',
  'forums.thread.new': '新しいスレッド',
  'forums.thread.delete': 'スレッドを削除する',
  'forums.post.new': '新規投稿',
  'forums.archived.forum': 'このフォーラムは{archivedAt, date, long}でアーカイブされました。フォーラムはまだ閲覧可能ですが、新しいコンテンツを作成することはできなくなりました。',
  'forums.category.archive': 'アーカイブカテゴリー',
  'forums.category.archiveExplained': 'スレッドがないカテゴリは削除され、内容のあるカテゴリは新しいスレッドが作成されないようになります。',
  'forums.category.reactivate': 'カテゴリの再活性化',
  'forums.category.isVotable': 'ユーザーがスレッドにアップ/ダウン投票できるようにする',
  'forums.category.titleIsGlobal': 'このカテゴリタイトルは、弊社がサポートしている場合、ユーザーの言語で表示されます。',
  'forums.category.languageLimited': 'このカテゴリは、この言語のユーザーにのみ表示されます。',
  'forums.threadsCounter': 'トピックディスカッションの総数',
  // Pre-defined category names
  'forums.category.intlName.general': '総合討論',
  'forums.category.intlName.announcements': 'お知らせ',
  'forums.category.intlName.stories': '物語',
  'forums.category.intlName.universes': '宇宙',
  'forums.category.intlName.universe': '宇宙',
  'forums.category.intlName.community': '地域社会',
  'forums.category.intlName.meetups': 'ミートアップ',
  'forums.category.intlName.characters': 'キャラクター',
  'forums.category.intlName.locations': '場所',
  'forums.category.intlName.history': '歴史',
  'forums.category.intlName.art': 'アート',
  'forums.category.intlName.inspiration': 'インスピレーション',
  'forums.category.intlName.news': 'ニュース',
  'forums.category.intlName.help': 'ヘルプ',
  'forums.category.intlName.questions': '質問',
  'forums.category.back': '掲示板の概要に戻る',
  // Forum restrictions
  'forums.forumRestriction.usersOnly': '登録されたユーザーのみアクセスを許可する',
  'forums.settings.accessControls': 'アクセス制御',
  'forums.forumRestrictionsMessages.usersOnlyForum': 'このフォーラムは、登録ユーザーのみがアクセスできます。',
  'forums.noThreads': '特に見るべきものはない。会話を始めるのはあなた次第です。',
  'forums.noPosts': 'まだ誰も返信していません。一番乗りを目指しましょう。',
  // Threads
  'forums.createThread': '新しいディスカッションを作成する',
  'forums.createThreadIn': '{categoryName}に新しいスレッドを作成します。',
  'forums.openingPost': 'オープニングポスト',
  'forums.createQuestionLabel': 'これは質問ですか？',
  'forums.thread.noReplies': '現在、ここに返信はありません。',
  'forums.reply': '返信',
  'forums.post': '送信',
  'forums.posting': '投稿中...',
  'forums.reset': 'リセット',
  'forum.thread.reply': 'このスレッドに返信を追加する',
  'forums.yourMessage': 'メッセージ',
  'forums.thread.replyPosted': 'メッセージが投稿されました。',
  'forums.thread.totalPosts': `{totalReplies, number}{totalReplies, plural,
    other {件の返信}
  }`,
  'forums.thread.createdAt': '{createdAt, date, medium}で作成'
};