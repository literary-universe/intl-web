// Notifications & flashnews
export default {
  'flashnews.create': '新しいニュースフラッシュを作成',
  'flashnews.startsAt': 'このメッセージの表示を開始：',
  'flashnews.endsAt': 'このメッセージの表示を停止：',
  'flashnews.newLanguage': '追加する新しい言語を選択する',
  'flashnews.onlyDisplayOn': 'ニュースは、以下で選択された言語（選択されている場合）のみに表示され、他の言語ではデフォルト言語であってもニュースは表示されません。'
};