export default {
    'plans.desc': '文学の宇宙のプランや特徴を説明しています。',
    'plans.title': '文学の宇宙をもっと活用しよう',
    'plans.text': '文学の宇宙は、基本的な機能を無料で提供しています。読書家やクリエイターの方には、アドバンス・メンバーシップをお勧めします。',
    'plans.faq.basicsPriced': 'なぜ無料会員の物が少ないのか？',
    'plans.faq.justification': '文学の宇宙では、お客様のプライバシーと知的財産を重視しています。そのため、お客様のデータを使って広告を販売したり、他社に提供したりすることはありません。そのため、多くのものを自分たちで作らなければなりません。これには多くの時間がかかりますし、運営コストもかかります。そこで私たちは、文学の宇宙小さなプレビューとして（それでも最も重要なことはできるようになっています）、無料メンバーシップを設定しました。複数のメンバーシップ層とアドオンを用意することで、皆様のニーズにお応えすると同時に、私たちがサービスを維持し、開発を続けることができるようにしています。もし、私たちが作っているものを気に入っていただけたなら、有料オプションへのサインアップをご検討ください。ありがとうございました。',
    'plans.limits': '制限',
    'plans.features': '特徴',
    'plans.account.user': '看客',
    'plans.account.user.desc': '試してみるための最初のトライアルアカウント。',
    'plans.account.explorer': '冒険者',
    'plans.account.explorer.desc': '愛好家や友人のために。',
    'plans.account.adventurer': '冒険家',
    'plans.account.adventurer.desc': 'クリエイターとサポーターのために。',
    'plans.account.storyteller': '語り部',
    'plans.account.storyteller.desc': '最も創造的で献身的な人のために。',
    'plans.unlimited': '無制限',
    'plans.price': '価格',
    'plans.price.free': '自由',
    'plans.price.monthly': '毎月',
    'plans.price.annually': '毎年',
    'plans.price.perMonth': ' / 月',
    'plans.price.perYear': ' / 年',
    'plans.development': '開発中',
    'plans.limits.encEntries': 'エンサイクロペディアのエントリー',
    'plans.limits.maxCollaborators': 'プロジェクトごとの協力者',
    'plans.limits.pmChats': 'プライベートメッセージの最大受信者数',
    'plans.feature.encCustomFields': 'エントリ上のカスタマイズ可能な百科事典フィールド',
    'plans.feature.encAdvCategories': 'その他の百科事典のカテゴリ',
    'plans.feature.scratchpad': 'メモ帳',
    'plans.feature.library': '図書館',
    'plans.feature.blog': 'パーソナルブログとユニバースブログ',
    'plans.feature.customBlogTheme': 'カスタマイズ可能なブログテーマ',
    'plans.feature.advanceStats': '高度な統計',
    'plans.feature.challenges': 'ライティングの課題',
    'plans.feature.newFeaturesVote': '新機能に投票する',
    'plans.feature.newFeaturesSuggest': '新機能の提案',
    'plans.feature.accessToBeta': 'ベータ版の機能にアクセス',
    'plans.feature.userGroups': 'ユーザーグループ',
    'plans.feature.privateUserGroups': 'プライベートおよび隠しユーザーグループの作成',
    'plans.feature.internalUniverseForums': '宇宙のための社内フォーラム',
    'plans.feature.fictionalTime': '架空の時間',
    'plans.feature.forums': 'ブログ&ユニバースページのフォーラム',
    'plans.feature.timelines': '宇宙のタイムライン',
    'plans.feature.webMonetization': 'ウェブマネタイズ',
    'plans.feature.storyMonetization': '物語のマネタイズ',
    'plans.feature.authorSupport': '著者サポート',
    'plans.feature.translations': '翻訳機能',
    'plans.feature.fanFiction': 'ファンフィクションの執筆・管理',
    'plans.feature.universeFanArt': 'ファンアートマネージャー',
    'plans.feature.overviewNotes': '物語の概要メモ',
    'plans.feature.personalNotesStoriesUniverses': '物語と宇宙に関する個人的なメモ',
    'plans.feature.storyReviews': '物語レビュー',
    'plans.feature.readingLists': 'リーディングリスト',
    'plans.feature.scenesManager': 'シーンマネージャー',
    'plans.feature.glossaries': '翻訳用語集',
    'plans.currency.select': '通貨を選択',
    'plans.subscription.ownAlready': '現在、このプランを持っています。',
    'plans.subscription.title': 'サブスクリプション',
    'plans.see': '利用可能なサブスクリプションプランを見る',
    'plans.current.none': 'あなたは現在、文学の宇宙をサポートしていません。',
    'plans.current.buying': 'ご購入内容の確認に時間がかかる場合があります。恐れ入りますが、ご了承ください。',
    'plans.current.heading': '現在の購読プラン',
    'payment.methods.title': 'ペイメント＆マネタイズ',
    'payment.methods.tabs': '支払い方法',
    'payment.option.title': '支払い方法',
    'payment.option.notFount': '決済方法が保存されていません。',
    'payment.option.create': '新しい支払い方法の追加',
    'payment.option.description': '保存されているお支払い方法の一覧です。ここで新しいお支払い方法を保存しておくと、チェックアウトが早くなります。リテラリー・ユニバースでは、お客様のクレジットカード番号などのお支払い情報を保存したり、処理したりすることはありません。',
    'payment.history.title': '支払履歴',
    'payment.history.tab': '支払履歴',
    'payment.history.none': '過去の支払いに関する記録はありません。',
    // Added 2020-04-30
    'plans.options.aria': '支払い方法',
    'plans.options.creditCard': 'クレジットカード',
    'plans.purchase.action': '購読する',
    'plans.purchase.title': '文学の宇宙のサブスクリプションを購入する',
    'payment.creditCard.details': 'クレジットカードの詳細',
    'payments.zipcode.label': '郵便番号',
    'payment.card.name': 'カードの名前',
    'plans.subscribed.thankYou': '文学の宇宙を支援していただきありがとうございます。',
    'plans.subscribed.periodEnd': 'ご利用期間は{end, date, medium}までです。',
    'plans.subscribed.renewal': `{method, select,
    charge_automatically {契約終了時には自動的に更新}
    send_invoice {終了時に請求書をお支払いいただくと、契約が更新}
    other {}
  }更新されます。`,
    'plans.subscribed.paymentMethod': '次のサイクルのお支払いには、お客様のデフォルトのお支払い方法が使用されます。',
    'plans.subscribed.status': `購読状況：{status, select,
    active {アクティブ}
    incomplete {何かが足りない}
    incomplete_expired {何かが足りない}
    trialing {トライアル}
    past_due {お支払いが滞っています}
    canceled {キャンセル}
    unpaid {未払い}
    other {}
  }`,
    'payments.changeMethod': '支払い方法の変更',
    'plans.subscribed.willNotRenew': 'は、サイクルの終わりには更新されません。',
    'plans.cancel.button': '購読解除',
    'plans.cancel.question': '月額プランをキャンセルしてもよろしいですか？お客様のサブスクリプションは、請求サイクルの最後まで有効です。',
    'plans.cancel.confirmationButton': 'はい、解約したいと思います。',
    'plans.cancel.failed': '申し訳ありませんが、現時点ではお客様のプランをキャンセルすることができませんでした。後でもう一度お試しください。',
    'plans.cancel.restoreBtn': 'サブスクリプションの復元',
    'plans.cancel.restoreFailed': '申し訳ありませんが、現時点では購読の復元ができませんでした。後でもう一度お試しください。',
    'plans.change.button': 'サブスクリプションの変更',
    'plans.change.explanation': 'アップグレードまたはダウングレードしたいプランを選択してください。差額分の料金がすぐに請求されます。',
    'plans.change.failed': '申し訳ありませんが、現時点ではお客様のプランを変更することができませんでした。後日、再度お試しください。',
    'plans.change.upgrade': 'アップグレード',
    'payments.disclaimer': 'お支払いの情報と処理は、Stripeを介して行われます。クレジットカードや銀行口座の情報は、当社のサーバーには一切保存されません。',
    'buy.payment.optionsSelect': '支払い方法の選択',
    'payments.card.number': 'カード番号',
    'payments.card.expire': '有効なスルー',
    'payments.card.cvv': 'CVV',
    'payments.card.info': '{cardType}カード、{ending}で終わる',
    'payments.none': '保存された支払い方法は見つかりませんでした。',
    'payments.added': '新しい支払い方法の追加に成功しました。',
    'payments.delete.confirm': '本当に{ending}で終わる{cardType}を削除したいのですか？',
    'payment.invoice.status': '請求書のステータス',
    'payment.invoice.statusMessage': `{status, select,
    draft {ドラフト}
    open {開}
    paid {有料}
    uncollectible {貸し倒れ}
    void {ボイド}
    other {}
  }`,
    'payment.invoice.id': '請求書ID',
    'payment.invoice.pdf': '請求書のpdf',
    'payment.invoice.total': '合計',
    'payment.invoice.what': '請求書の説明',
    'payment.stripe.powered': 'Powered by ',
    'payment.listMethods.card': 'カード',
    'payment.expiresAt': '期限は{month}/{year}です。',
    'payment.card.details': `{last4}で終わる{type, select,
    credit {クレジット・カード}
    debit {デビットカード}
    other {カード}
  }`,
    'payment.options.default': 'デフォルト',
    // Added 2020-05-27
    'payment.method.add': '新しい支払方法の追加',
    'payment.methods.adding': 'お客様のアカウントに新しいお支払い方法が追加されました。',
    'payment.methods.delete': '決済方法の削除',
    'payment.upgradePlan.option': '{plan} - {price}',
    'payment.methods.webMonetization': 'ウェブマネタイズ',
    'settings.webMonetization.placeholder': 'お支払い方法',
    'payment.webMonetization.explained': 'Web Monetizationは、ウェブ決済の標準規格として提案されています。ここでは、あなたのプロフィールページ、ブログ、ユニバース、ストーリーに、Webマネタイズで利用される決済ポインタを追加することができます。',
    'payment.webMonetization.learnMore': 'ウェブマネタイズについて詳しくはこちら'
};