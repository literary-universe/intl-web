export default {
    'universe.authors.title.meta': '{title} autores',
    'universe.fanfiction.see': 'Todos los fánfics para {universe}',
    'universe.fanfiction.desc': 'Fánfic para {universe}',
    'universe.fanfiction.for': 'Fánfic de {universe}',
    'universe.fanarts.for': '{universe} Fan Artes',
    'universe.fansettings.for': 'Ajustes de fans de {universe}',
    'universe.fanfiction.stories': 'Historias',
    'universe.fanfiction.fanarts': 'Fan Artes',
    'universe.stories.count': `{num, plural,
    =0 {No hay historias}
    one {Hay una historia}
    other {Hay # historias}
    } en este universo.`,
    'universe.authors.title': 'Autores',
    'universe.authors.desc': 'Creadores y colaboradores de {universe}',
    'universe.contributors': 'Colaboradores',
    'universe.translators': 'Traductores',
    'universe.authors.notice': 'Cada historia podría tener más personal que haya trabajado en ella. Para saber más, mira los detalles de la historia.',
    'universe.createdBy': `{gender, select,
    male {Creado por}
    female {Creada por}
    other {Creación de}} `,
    'universe.stories.for': 'Historias para {universe}',
    'universe.stories.own': 'Historias de {universe}',
    'universe.stories.desc': 'Historias canónicas para {universe}',
    'universe.encyclopedia.title': 'Enciclopedia de {universe}',
    'universe.encyclopedia.desc': 'Todos los detalles para {universe}',
    'universe.fanArt.submit': 'Envíe su arte',
    'universe.fanArtHeaderDesc.item': 'Fan art de {universe} - {name}',
    'universe.fanArtHeaderDesc.submit': '{universe} envíos de fan art',
    'universe.fanArtHeaderDesc.overview': 'Fan art de {universe}'
};