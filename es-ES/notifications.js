// Notifications & flashnews
export default {
  'flashnews.create': 'Crear nuevo flash de noticias',
  'flashnews.startsAt': 'Comienza a mostrar este mensaje en',
  'flashnews.endsAt': 'Deje de mostrar este mensaje en',
  'flashnews.newLanguage': 'Seleccione el nuevo idioma a añadir',
  'flashnews.onlyDisplayOn': 'Las noticias se mostrarán sólo en los idiomas seleccionados a continuación (si se selecciona alguno), no se mostrarán noticias, ni siquiera en el idioma por defecto, en otros idiomas.'
};