export default {
    'user.friend.request.add': 'Añadir a amigos',
    'user.friend.requests': `Tienes {num, number} amigo nuevo {num, plural,
    one {solicitud}
    other {solicitudes}
  }`,
    'user.friend.unfriend': 'Quitar de amigos',
    'user.block': 'Bloquear',
    'user.unblock': 'Desbloquear',
    'user.joined': `{gender, select,
    male {¡Se unió a}
    female {¡Se unió a}
    other {¡Se unió a}} en {date, date, long}`,
    'user.friend.request.accept': 'Aceptar petición de amistad',
    'user.friend.request.cancel': 'Cancelar petición de amistad',
    'user.friend.request.deny': 'Rechazar amistad',
    'user.listing': 'Listado de usuarios',
    'user.profile.visit': 'Visitar el perfil de {user}.',
    'user.profile.avatar': 'Avatar de {username}',
    'user.friend.list.requests': 'Peticiones de amistad',
    'user.friend.norequests': 'No tienes peticiones de amistad.',
    'user.dashboardWelcome': `{dayPart, select,
    morning {Buenos días}
    afternoon {Buenas tardes}
    evening {Buenas noches}
    night {Buenas noches}
    other {Buenas noches}
  } {displayName}!`,
    'user.verification.publishedAuthor': 'Este autor ha sido publicado de forma tradicional',
    'user.verification.luAuthor': 'Autor destacado del Universo Literario',
    'user.verification.luEmployee': 'Empleado del Universo Literario',
    'user.verification.publicFigure': 'Personaje público verificado'
};