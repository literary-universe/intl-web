export default {
  'roadmap.ariaTimeOptions': 'Selección de elementos de la hoja de ruta en función de su estado.',
  'roadmap.state.backlog': 'Backlog',
  'roadmap.state.inprogress': 'En progreso',
  'roadmap.state.preview': 'Vista previa',
  'roadmap.state.beta': 'Beta',
  'roadmap.state.ga': 'Disponibilidad general',
  'roadmap.eta.q1.short': 'P1',
  'roadmap.eta.q2.short': 'P2',
  'roadmap.eta.q3.short': 'P3',
  'roadmap.eta.q4.short': 'P4',
  'roadmap.eta.q1.long': 'Primer trimestre',
  'roadmap.eta.q2.long': 'Segundo trimestre',
  'roadmap.eta.q3.long': 'Tercer trimestre',
  'roadmap.eta.q4.long': 'Cuarto trimestre',
  'roadmap.backlog': 'Backlog',
  'roadmap.history': 'Historia de la evolución',
  'roadmap.item.inBacklog': 'Tenemos previsto desarrollar esta función en el futuro.',
  'roadmap.item.inProgress': 'El desarrollo de la función comenzó {startedAt, date}.',
  'roadmap.item.inBeta': 'Esta función ha entrado en fase beta en {betaAt, date}.',
  'roadmap.item.inPreview': 'Esta función está disponible para los suscriptores de Storyteller desde {previewAt, date}.',
  'roadmap.item.inGa': 'Esta función está disponible de forma generalizada desde {gaAt, date}.',
  'roadmap.item.originalIssue': 'Esta función fue sugerida originalmente por nuestros usuarios.',
  'roadmap.eta.mapTitle': `¡Planeado para {quarter, select,
    Q1 {P1}
    Q2 {P2}
    Q3 {P3}
    Q4 {P4}
    other {}
  } {year, date, ::yyyy}`,
  'roadmap.eta.mapTitleFinished': 'Finalizado en {time, date, ::MMMM ::yyyy}',
  'roadmap.back': 'Volver a la hoja de ruta',
  'roadmap.seeBacklog': 'Vea qué otras cosas tenemos previsto hacer',
  'roadmap.seeFinished': 'Si quiere profundizar en nuestra historia, tenemos un buen resumen preparado para usted',
  'roadmap.planned': 'Planificación',
  'roadmap.finished': 'Completado'
};